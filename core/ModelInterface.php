<?php namespace Core;

interface ModelInterface {
    // public function __construct ($data = []);
    public function setConnectionId($connectionsId);
    /**
     * Define a tabela do model
     *
     * @param String $tableName
     * @return void
     */
    public function setTable($tableName);
    /**
     * Define o campo chave primary
     *
     * @param String $fieldName
     * @return void
     */
    public function setPrimaryKey($fieldName);

    public function getTable();
    public function getPrimaryKey();

    /**
     * Lista simples em array dos campos do model
     *
     * @return void
     */
    public function getFields();
    /**
     * Executa INSERT INTO no banco
     *
     * @param Array $data Array com os valores ou NULL pagar direto dos paramentos anteriormente setados
     * @return Int Novo Id
     */
    public function insert ($data = null);
    /**
     * Executa UPDATE no banco
     *
     * @param Int $id
     * @param Array $data Array com os valores ou NULL pagar direto dos paramentos anteriormente setados
     * @return void
     */
    public function update ($id, $data = null);
    public function setFieldDefault ($fieldName, $default = null, $permitNull = true);
    public function setFields ($fields);
    public function select ($fields = '*');
    public function delete ($where, $order = null, $limit = 1);

    /**
     * Busca registro pelo primaryKey
     *
     * @param Int|String|Array $where Valor do primaryKey, Array para outros parametros para o WHERE
     * @param Array $bindData BindData
     * @param String $order
     * @return this|Boolean False se não encontrar
     */
    public static function find ($where, $bindData = [], $order = null);

    /**
     * Busca uma lista de resultados
     *
     * @param Int|String $where
     * @param Array $bindData BindData
     * @param String $order
     * @param String $limit
     * @return Boolean|\PDOStatement False se erro
     */
    public static function findAll ($where = null, $bindData = [], $order = null, $limit = null);

    /**
     * Executa query sql
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function query ($sql, $bindData = []);


    /**
     * Executa query de select sql (fetchmode: class)
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function querySelect ($sql, $bindData = []);
    /**
     * Conta os registros da tabela
     *
     * @param Int|String|Array $where
     * @param Array $bindData
     * @param String $joins
     * @return Int|Boolean False se Erro
     */
    public static function sizeof ($where = null, $bindData = [], $joins = null);

    /**
     * Retorna dados como array
     *
     * @return Array
     */
    public function setData ($data);
    public function toArray();
}