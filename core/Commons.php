<?php namespace Core;

class Commons {
    public static function loadLib ($name) {
        $files = [
            \PATH."lib/{$name}",
            \PATH."lib/{$name}.php",
            \PATH."lib/{$name}/{$name}",
            \PATH."lib/{$name}/{$name}.php"
        ];

        foreach ($files as $file) {
            if (is_file($file)) {
                require_once($file);
                return true;
            }
        }
        return false;
    }
}