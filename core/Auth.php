<?php namespace Core;

use Models\Usuario;
use Models\Cliente;

class Auth {
    static $userData;
    public static function isAuth ()
    {
        $user = Auth::get();
        return $user != false;
    }

    public static function isUsuario ()
    {
        $user = Auth::get();
        return $user != false && $user->usuario_status >= 0;
    }

    public static function isCliente ()
    {
        $user = Auth::get();
        return $user != false && $user->cliente_status >= 0;
    }

    public static function getIdUsuario ()
    {
        $user = Auth::get();
        return !$user || !($user->usuario_status >= 0) ? false : $user->usuario_id;
    }

    public static function getIdGrupo ()
    {
        $user = Auth::get();
        return !$user || !($user->usuario_status >= 0) ? false : $user->grupo_id;
    }

    public static function getIdCliente ()
    {
        $user = Auth::get();
        return !$user || !($user->cliente_status >= 0) ? false : $user->cliente_id;
    }

    /**
     * Pega UserData do usuario logado
     *
     * @return User
     */
    public static function get ()
    {
        if (!is_null(Auth::$userData)) {
            return Auth::$userData;
        }
        
        if (!isset($_SESSION['AUTH']) || !$_SESSION['AUTH']) return false;
        $userData = new UserData();
        if (!is_null($_SESSION['AUTH_USUARIO'])) {
            $usuario = Usuario::find($_SESSION['AUTH_USUARIO']);

            if ($usuario != false) {
                $userData->usuario_status = $usuario->status;
                $userData->usuario_id = $usuario->id;
                $userData->grupo_id = $usuario->grupo_id;
                $userData->imagem = $usuario->imagem;
                $userData->nome = $usuario->nome;
                $userData->telefone = $usuario->telefone;
                $userData->celular = $usuario->celular;
                $userData->email = $usuario->email;
                $userData->data_create = $usuario->data_create;
                $userData->data_update = $usuario->data_update;
            } else {
                $userData->usuario_status = -1;
            }
        }
        if (!is_null($_SESSION['AUTH_CLIENTE'])) {
            $cliente = Cliente::find($_SESSION['AUTH_CLIENTE']);

            if ($cliente != false) {
                $userData->cliente_status = $cliente->status;
                $userData->cliente_id = $cliente->id;
                $userData->imagem = $cliente->imagem;
                $userData->nome = $cliente->nome;
                $userData->telefone = $cliente->telefone;
                $userData->celular = $cliente->celular;
                $userData->email = $cliente->email;
                $userData->data_create = $cliente->data_create;
                $userData->data_update = $cliente->data_update;
            } else {
                $userData->cliente_status = -1;
            }
        }

        // Caso o usuario e cliente esteja inativo
        if ($userData->usuario_status < 1 && $userData->cliente_status < 1) {
            $_SESSION['AUTH'] = false;
            $_SESSION['AUTH_USUARIO'] = null;
            $_SESSION['AUTH_CLIENTE'] = null;

            return false;
        }
        Auth::$userData = $userData;
        return Auth::$userData;
    }

    public static function login ($email, $senha)
    {
        $where = ['email' => $email, 'senha' => md5($senha)];
        $usuario = Usuario::find($where);
        $cliente = Cliente::find($where);
        $response = ['status' => 0, 'message' => '', 'redirect' => \URL.'login'];

        $statusUsuario = ($usuario!=false) ? $usuario->status : -1;
        $statusCliente = ($cliente!=false) ? $cliente->status : -1;

        $userData = new UserData();
        $userData->usuario_status = $statusUsuario;
        $userData->cliente_status = $statusCliente;
        $userData->data_acesso = date('Y-m-d H:i:s');

        $_SESSION['AUTH'] = false;
        $_SESSION['AUTH_USUARIO'] = null;
        $_SESSION['AUTH_CLIENTE'] = null;

        if ($statusUsuario > 0) {
            $_SESSION['AUTH'] = true;
            $_SESSION['AUTH_USUARIO'] = $usuario->id;
            $userData->usuario = $usuario->id;
            $userData->grupo_id = $usuario->grupo_id;
            $userData->imagem = $usuario->imagem;
            $userData->nome = $usuario->nome;
            $userData->telefone = $usuario->telefone;
            $userData->celular = $usuario->celular;
            $userData->email = $usuario->email;
            $userData->data_create = $usuario->data_create;
            $userData->data_update = $usuario->data_update;
        }

        if ($statusCliente > 0) {
            $_SESSION['AUTH'] = true;
            $_SESSION['AUTH_CLIENTE'] = $cliente->id;
            $userData->cliente_id = $cliente->id;
            $userData->imagem = $cliente->imagem;
            $userData->nome = $cliente->nome;
            $userData->telefone = $cliente->telefone;
            $userData->celular = $cliente->celular;
            $userData->email = $cliente->email;
            $userData->data_create = $cliente->data_create;
            $userData->data_update = $cliente->data_update;
        }

        if ($_SESSION['AUTH']) {
            $response['status'] = 1;
            $response['message'] = 'Logado com sucesso';
            Auth::$userData = $userData;
            if ($statusUsuario > 0) {
                $response['redirect'] = \URL.'admin';
            } else {
                $response['redirect'] = \URL.'minha-area';
            }
        } else {
            Auth::$userData = null;
            $response['status'] = 0;
            
            if ($statusCliente == 0) {
                $response['message'] = 'Cliente inativo, entre em contato com o suporte.';
            } elseif ($statusUsuario == 0) {
                $response['message'] = 'Usuário inativo, entre em contato com o administrador.';
            } else {
                $response['message'] = 'Email ou senha incorretas';
            }
        }

        return $response;

    }

    public static function logout ()
    {
        return session_destroy();
    }
}