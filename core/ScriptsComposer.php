<?php
namespace Core;

class ScriptsComposer {
    public static function postInstall ()
    {
        $path = realpath('./');
        if (!is_dir($path."/upload")) {
            echo "Create dir: ".$path."/upload";
            mkdir($path."/upload", 0777);
        }
        if (!is_dir($path."/tmp")) {
            echo "Create dir: ".$path."/tmp";
            mkdir($path."/tmp", 0777);
        }
        
        file_put_contents($path."/tmp/.htaccess", "Deny from all\n");
    }
}