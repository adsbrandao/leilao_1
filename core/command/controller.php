CREATE CONTROLLER ---

<?php
use Core\StringCase;

$pathControllers = 'app/Controllers/';
$namespaceControllers = 'App\\Controllers';

$controller = StringCase::pascalCase($params['controller'] ?? $params[0]);

if (substr($controller, -10) != 'Controller') $controller .= "Controller";

$file = $pathControllers . $controller . '.php';
if (is_file(PATH . $file)) {
    echo "ERROR: Controller já existe.\n";
    exit;
}

$controller = explode('/', $controller);
$controllerName = array_pop($controller);

if (count($controller) > 0) {
    $namespaceControllers .= "\\".implode("\\", $controller);
}
$controller = $controllerName;

$content = "<?php namespace {$namespaceControllers};\n\n".
    "use Core\\Controller;\n".
    "class {$controller} extends Controller {\n\n";
$content .= "    // public function index () {}\n".
    "}\n";

echo "SAVE {$file}\n\n";
file_put_contents(PATH . $file, $content);
