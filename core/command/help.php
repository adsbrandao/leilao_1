HELP

<?=json_encode($params);?>

Comandos:

generate                Gera os models e modelBase de acordo com o
                        banco de dados.

resource                Gera o ControllerResource
                        Exemplo:
                        php cli.php resource Clientes ClienteModel

controller              Gera o Controller
                        Exemplo:
                        php cli.php controller Clientes

help                    Este

exit;
