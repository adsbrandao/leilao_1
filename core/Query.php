<?php namespace Core;

class Query {
    /**
     * 
     * @var \PDO $connections
     */
    static $connections = [];
    static $cfgConnections = [];
    static $tables = [];
    /**
     * 
     * @param string $dbname Nome do banco de dados
     * @param int $connectionsId Id da connection DEFAULT=NULL
     * @param string $host Hostname do banco de dados
     * @param string $port Porta do banco DEFAULT=NULL (3306)
     * @param string $username Usuario do Banco
     * @param string $password Senha do usuario
     * @param string $driver Driver de accesso ao banco DEFAULT NULL (Somente: mysql)
     * @throws \Exception
     * @return boolean
     */
    static function setConnection($dbname,$connectionsId = null,$host = NULL,$port=NULL,$username = NULL,$password = NULL,$driver = NULL){
        $connectionsId = $connectionsId??0;
        $driver = $driver??'mysql';
        $host = $host??'localhost';
        $port = $port??'3306';
        $username = $username??'root';
        $password = $password??'';
        
        $driversAceitos = array('mysql');
        if(!in_array($driver,$driversAceitos)){
            throw new \Exception("Driver Driver '{$driver}' not accepted.");
            return false;
        }
        
        self::$cfgConnections[$connectionsId] = array(
            'driver'=>$driver,
            'host'=>$host,
            'port'=>$port,
            'dbname'=>$dbname,
            'username'=>$username,
            'password'=>$password
        );
        return true;
    }
    /**
     * 
     * @param int $connectionsId Id da connection DEFAULT=NULL
     * @param boolean $autoConnect Connect if not conected
     * @throws \Exception
     * @return \PDO|boolean
     */
    static public function connect($connectionsId = null, $autoConnect = true){
        $connectionsId = $connectionsId??0;
        
        if(isset(self::$connections[$connectionsId]))
            return self::$connections[$connectionsId];
        
        if(!$autoConnect)
            return false;
        
        if(!isset(self::$cfgConnections[$connectionsId])){
            throw new \Exception("Banco de dados não configurado");
            return false;
        }
        
        $driver = self::$cfgConnections[$connectionsId]['driver'];
        $host = self::$cfgConnections[$connectionsId]['host'];
        $port = self::$cfgConnections[$connectionsId]['port'];
        $dbname = self::$cfgConnections[$connectionsId]['dbname'];
        $username = self::$cfgConnections[$connectionsId]['username'];
        $password = self::$cfgConnections[$connectionsId]['password'];
        
        $options = array();
        $options[\PDO::ATTR_PERSISTENT] = false;
        if($driver=="mysql"){
           //$options[\PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES 'UTF8'";
            $options[1002] = "SET NAMES 'UTF8'";
        }
        
        try {
            self::$connections[$connectionsId] = new \PDO(
                "{$driver}:host={$host};port={$port};dbname={$dbname};charset=UTF8",
                $username,
                $password,
                $options
            );
            self::$connections[$connectionsId]->exec("SET time_zone = 'America/Sao_Paulo'");
        } catch (\PDOException $e) {
            self::$connections[$connectionsId] = null;
            throw new \Exception("Connection failed: " . utf8_encode( $e->getMessage() ).".");
            return false;
        }
        
        return self::$connections[$connectionsId];
    }
    static public function beginTransaction($connectionsId = null){
        $pdo = static::connect($connectionsId);
        return $pdo->beginTransaction();
    }
    static public function commit($connectionsId = null){
        $pdo = static::connect($connectionsId);
        return $pdo->commit();
    }
    static public function rollBack($connectionsId = null){
        $pdo = static::connect($connectionsId);
        return $pdo->rollBack();
    }
    /** Execulta query sql
     * 
     * @param string $sql
     * @param array $bindData Array com os valores para o bind.
     * @param int $connectionsId Id da connection DEFAULT=NULL
     * @throws \Exception
     * @return boolean|\PDOStatement
     */
    static public function query($sql,$bindData = null,$connectionsId = null){
        $pdo = static::connect($connectionsId);
        $debug = \MODO == 'DEBUG';
        $fileDebug = \PATH . "debug.txt";
        // DEBUG
        if ($debug) {
            $f = fopen($fileDebug, 'a');
            fwrite($f, "\n# SQL\n");
            fwrite($f, $sql . "\n");
        }
        // END DEBUG */
        $timeStart = microtime(true);
        if(is_null($bindData) || empty($bindData)){
            if(!$query = $pdo->query($sql)){
				list($handle, $codError, $StrError) = $pdo->errorInfo();
				if ($debug) {
                    $timeEnd = microtime(true);
                    fwrite($f, "# ERROR: {$codError} - {$StrError}\n");
                    fwrite($f, "# timeExec: " . ($timeEnd - $timeStart) . "\n");
                    fclose($f);
                }
				throw new \Exception("Error: #{$codError}: {$StrError}<br />\r\n".$sql,$codError);
                return false;
            } elseif ($debug) {
                $timeEnd = microtime(true);
                fwrite($f, "# timeExec: " . ($timeEnd - $timeStart) . "\n");
                fclose($f);
            }
        } else {
            $query = $pdo->prepare($sql);
            if ($debug) {
                foreach ($bindData as $parameter=>$value){
                    fwrite($f, "# {$parameter} = '{$value}'\n");
                }
            }
            
            if(!$query->execute( $bindData )){
                list($handle, $codError, $StrError) = $query->errorInfo();
				if ($debug) {
                    $timeEnd = microtime(true);
                    fwrite($f, "\n# ERROR: {$codError} - {$StrError}\n");
                    fwrite($f, "# timeExec: " . ($timeEnd - $timeStart) . "\n");
                    fclose($f);
                }
                throw new \Exception("Error: #{$codError}: {$StrError}<br />\r\n".$query->queryString,$codError);
                return false;
            } elseif ($debug) {
                $timeEnd = microtime(true);
                fwrite($f, "# timeExec: " . ($timeEnd - $timeStart) . "\n");
                fclose($f);
            }
        }
		
        return $query;
    }
    static public function isTable($table,$connectionsId = null)
    {
        return in_array($table, static::getTables($connectionsId));
    }
    /**
     * 
     * @param int $connectionsId Id da connection DEFAULT=NULL
     * @return boolean|string
     */
    static public function lastInsertId($connectionsId = null,$name=null){
        $pdo = static::connect($connectionsId);
        
        return ((!$pdo)? false : $pdo->lastInsertId($name));
    }
    static public function getTables ($connectionsId = null)
    {
        if (!isset(static::$tables[$connectionsId])) {
            $result = static::query("SHOW TABLES",null,$connectionsId);
            static::$tables[$connectionsId] = [];
            while($row = $result->fetch()) {
                static::$tables[$connectionsId][] = $row[0];
            }
        }

        return static::$tables[$connectionsId];
    }
    static public function getFields ($table, $connectionsId = null)
    {
        $result = Query::query("SELECT * FROM `{$table}` WHERE 0 LIMIT 1", null, $connectionsId);
        $c = $result->columnCount();
        $fields = [];
        for($i=0;$i<$c;$i++){
            $field = $result->getColumnMeta($i);

            if (is_null($field['name'])) {
                continue;
            }

            $fields[] = $field['name'];
        }

        return $fields;
    }



    static public function transformaWhere ($primaryKey, &$where = null, &$bindData = [])
    {
        $Where = '1=1';
        if (is_numeric($where)) {
            $Where = $primaryKey . ' = '. $where;
            $bindData[] = $where;
        } elseif (is_array($where)) {
            $bindData = [];
            $bindData = [];
            foreach ($where as $n=>$v) {
                if (is_string($n)) {
                    $Where .= ' AND '.$n.' = ?';
                    $bindData[] = $v;
                } else {
                    $Where .= ' AND '.$v;
                }
            }
        } else {
            $Where = is_null($where)? '1=1' :$where;
        }
        $where = $Where;
    }
}
