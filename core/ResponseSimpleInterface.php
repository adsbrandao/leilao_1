<?php namespace Core;

interface ResponseSimpleInterface {
    /**
     *
     * @param String|Array $body Payload da resposta
     * @param Int $httpCode Numero correspondente ao code HTTP da resposta.
     * @param array $headers Array com headers adicionais
     */
    public function __construct ($body, $httpCode = 200, $headers = []);
    /**
     * Add header
     *
     * @param String $name Exemplo Content-type
     * @param String $value Exemplo text/html
     * @return void
     */
    public function header ($name, $value);

    /**
     * Envia a resposta
     *
     * @return void
     */
    public function send ();
}