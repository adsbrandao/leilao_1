<?php 
namespace Core;

use Core\StringCase;

class DataTable {
  private $_recordsFiltered;
  private $_recordsTotal;
  private $_error;
  private $_data = array();
  private $_params = array();
  private $_columns = array();
  private $_rowActions = array();
  private $_log = '';
  private $_where = null;
  private $_joins = '';
  /**
   * @var \Core\Model
   */
  private $_model;
  
  public function __construct ()
  {
    //$request->start
    $params = [
      'start' =>  $_GET['start']??null,
      'length' => $_GET['length']??null,
      'search' => $_GET['search']??null,
      'order' =>  $_GET['order']??null
    ];

    if (is_null($params['order'])) {
      $params['order'] = []; // order[i][column] e columns[i][dir] (asc ou desc)
    }
  
    $this->_params = $params;
  }

  public function where ($where)
  {
    if (is_null($this->_where)) {
      $this->_where = $where;
    } else {
      $this->_where .= ' AND '.$where;
    }
  }

  public function addJoin ($table, $on, $type = 'LEFT')
  {
    $TYPE = in_array($TYPE, ['INNER', 'LEFT', 'RIGHT']) ? $TYPE : 'LEFT';
    $this->_joins .= ' '.$TYPE . ' JOIN '.$table.' ON '. $on;
  }

  /**
   * acrescenta um orderBy
   * 
   * @param  String $columnName Name da coluna
   * @para String $dir Direcao use asc ou desc
   */
  public function order_by ($columnName, $dir = 'asc')
  {
    $this->_params['order'] = ['column' => $columnName, 'dir' => $dir];
  }

  /**
   * Adiciona coluna
   * 
   * @param String $name Nome da  coluna
   * @param String $nameModel Nome da  coluna no Model (exemplo descricao ou z.descricao),
   * Caso deixar null entende-se que sera o mesmo nome da coluna.
   * @param String $label Label da coluna Se deixar null entende-se que sera o mesmo nome da coluna
   * @param Boolean $sortable Se coluna é ordenavel
   * @param Boolean $searchable Se coluna pode ser feita pesquisa
   * @param Callback $fncTransform Funcao altera o valor final da coluna, pode ser usado para mascarar.
   *    function ($valueColumn, $dataRow) { return $valueColumn; }
   *  
   */
  public function addColumn ($name, $nameModel = null, $label = null, $sortable = true, $searchable = true, $fncTransform = null)
  {
    if (is_null($nameModel)) $nameModel = $name;
    if (is_null($label)){
      $label = StringCase::splitStringCases($name);
      $label = implode(' ', $label);
      $label = ucwords($label);
    }

    if (is_null($fncTransform)) {
      $fncTransform = function ($value, $dataRow) {
        return (is_null($value=== null) || $value == '')? '-' : $value;
      };
    }

    $this->_columns[] = compact(
      'name', 'nameModel', 'label', 'sortable', 'searchable', 'fncTransform'
    );
  }

  /**
   * Adicionar um botao de acao em cada linha
   * 
   * @param String $name Nome do campo usado como id.
   * @param String $nameModel Nome do campo (no model,exemplo z.id) usado como id.
   * @param String $icon Icone do botao
   * @param String $title Titulo (ao passar o mouse)
   * @param String $hrefFormat href do botao, inserir {id} onde queira subtituir.Ex.: /produto/{id}
   * @param String $onclickFormat Texto do attribruto onclick (similar ao hrefFormat)  
   */
  public function addRowAction ($name, $nameModel, $icon, $title, $hrefFormat = null, $onclickFormat = null)
  { 
    $this->_rowActions[] = compact(
      'name', 'nameModel', 'icon', 'title', 'hrefFormat', 'onclickFormat'
    );

  }

  /**
   * Altera um param de entrada
   * 
   * @param String $name
   * @param Any $value
   */
  public function setParam ($name,$value = null)
  {
    $this->_params[$name]  = $value;
  }

  /**
   * Traz valor de um parametro de entrada
   */
  public function getParam ($name, $default = null)
  {
    return $this->_params[$name]  ?? $default;
  }

  /**
   * Define param orderDefault
   * 
   * @param String $column
   * @param String $dir Direcaoda ordem use: asc ou desc
   */
  public function setOrderDefault ($column, $dir = 'asc')
  {
    $this->_params['orderDefault'] = [
      'column' => $column,
      'dir' => $dir
    ];
  }

  /**
   * Traz lista de ordenacao
   * 
   * @return Array Array de array(column, dir)
   */
  public function getOrder ()
  {
    $this->_params['order'];

    $listColumns = [];
    $i = 0;
    foreach ($this->_columns as $columnData) {
      $listColumns[$i] = $columnData['nameModel'];
      $listColumns[$columnData['name']] = $columnData['nameModel'];
      $i++;
    }

    $orderList = [];
    
    if(count($this->_params['order']) == 0) {
      $orderList[] = $this->_params['orderDefault']?? [];
    } else {
      $orderList = $this->_params['order'];
    } 

    foreach($orderList as $i=>$item) {
      if (isset($listColumns[$item['column']])) {
        $name = $listColumns[$item['column']];
      } else {
        $name = '1';
      }
      $orderList[$i] = ['column' => $name, 'dir' => $item['dir'] ?? 'ASC'];
    }

    return $orderList;
  }

  /**
   * Define model deste DataTable
   * Obrigatoria caso use o metodo exec
   * ou Pode definir manual os dados com setData
   * 
   * @param \Core\Model $model 
   */
  public function setModel ($model)
  {
    $this->_model = $model;
  }

  /**
   * Define a data de retorno (lista de dados)
   * 
   * @param Array $data Array de array asssociativo ( data[i][{nomeColuna}] )
   */
  public function setData ($data)
  {
    $this->_data = $data;
  }

  /**
   * Transformar resultados de saida (fncTransform)
   * 
   * @return Array de array associativo ( data[i][{nomeColuna}] )
   */
  public function getData ()
  {
    return $this->_data;
  }

  private function _transformData ()
  {
    $data = $this->_data;
    $rowActions = $this->_rowActions;

    foreach ($data as $i => $dataRow) {
      // Inserir botoes de acao
      if (count($rowActions) > 0) {
        $rowActionBtns = array_reduce($rowActions, function($anterior, $atual) use ($dataRow) {
          if (is_null($atual['hrefFormat'])) {
            $novoBtn = '<a class="btn btn-default" onclick="'.$atual['onclickFormat'].'"><i class="'.$atual['icon'].'" aria-hidden="true"></i></a> ';
          } else {
            $novoBtn = '<a class="btn btn-default" href="'.$atual['hrefFormat'].'" title="'.$atual['title'].'"><i class="'.$atual['icon'].'" aria-hidden="true"></i></a> ';
          }
          $novoBtn = str_replace('{id}', $dataRow[ $atual['name'] ], $novoBtn);
          return $anterior . $novoBtn;
        }, '');


        $rowAction = [
          'row_action' => '<nobr>'.$rowActionBtns.'</nobr>'
        ];
        $data[$i] = array_merge($dataRow, $rowAction);
      }

      // Transforma colunas
      foreach ($this->_columns as $column) {
        $columnName = $column['name'];
        $columnValue = $dataRow[ $columnName ] ?? '';
        if (isset($column['fncTransform']) && is_callable($column['fncTransform'])) {
          $columnValue = $column['fncTransform']($columnValue, $dataRow);
        } elseif($columnValue=='') {
          $columnValue = '-';
        }
        $data[$i][$columnName] = $columnValue;
      }
    }

    return $data;
  }

  /** 
   * Executa select no Model
   * pegando os dados e inserindo em _data
   */
  public function exec ()
  {
    if (is_null($this->_model)) {
      throw new \Exception("Model não definido");
    }

    $columns = $this->_columns;

    $whereSearch = "";
    $bindData = [];
    if (is_null($this->_where)) {
      $whereSearch = "1=1";
    } else {
      $whereSearch = "(".$this->_where.")";
    }

    $search = $this->getParam('search', ['value' => '']);
    if (!isset($search['value']) || empty($search['value'])) {
      $search = false;
    } else {
      $search = str_replace("'", "", $search['value']);
      $whereSearch .= " AND 1=0";
    }

    foreach ($this->_rowActions as $rowAction) {
      $columns[] = ['name' => $rowAction['name'], 'nameModel' => $rowAction['nameModel']];
    }
    
    foreach ($columns as $column) {
      if ($search!== false) {
        $whereSearch .= " OR ".$column['nameModel']." LIKE ?";
        $bindData[] = '%' . $search . '%';
      }
    }

    $select = array_reduce($columns,function ($anterior,$atual) {
      if (is_null($anterior)) {
        $anterior = '';
      } else {
        $anterior .= ', ';
      }

      $anterior .= "IFNULL(".$atual['nameModel'].", '-')";

      $anterior .= ' as `'.$atual['name'].'`';

      return $anterior;
    });

    $orderList = $this->getOrder();
    $orderBy = "";
    $sep = ' ORDER BY ';
    foreach ($orderList as $orderData) {
        $orderBy .= $sep . ' '.$orderData['column'] .' '.$orderData['dir'];
        $sep = ',';
    }

    $this->_log .= $whereSearch;

    $sql = "SELECT {$select} FROM ".$this->_model->getTable();
    $sql .= $this->_joins;
    $sql .= " WHERE ".$whereSearch;
    $sql .= ' '.$orderBy;
    $length = $this->getParam('length', 25);
    $start = $this->getParam('start', 0);
    if ($length > 0) {
        $sql .= " LIMIT " . $start . ', ' . $length . ' ';
    }

    $res = $this->_model->query($sql, $bindData);
    $nResults = !$res ? 0 : $res->rowCount();
    $this->_recordsFiltered = $nResults;

    $this->_data = $nResults == 0 ? [] : $res->fetchAll(\PDO::FETCH_ASSOC);
    $this->_recordsTotal = $this->_model->sizeof($whereSearch, $bindData, $this->_joins);
  }

  /**
   * Traz os dados com as contagens, em json
   * @see https://datatables.net/manual/server-side#Returned-data
   * @return String JSON
   */
  public function jsonData ()
  {

    header('Content-type: text/json;Chaset=utf-8');
    return json_encode($this->toArray());
  }
  public function toArray ()
  {
    $return = array (
      'recordsFiltered' => $this->_recordsFiltered,// count($this->_data),
      'recordsTotal' => $this->_recordsTotal,
      'length' => count($this->_data),
      'log' => $this->_log,
      'data' =>  $this->_transformData()
    );

    if (is_null($return['recordsTotal'])) {
      $return['recordsTotal'] = $return['recordsFiltered'];
    }
    return $return;
  }
}