<?php namespace Core;

class ControllerResource {
    private $_title;
    private $_model;
    private $_baseUrl;
    private $_columns = [];
    private $_jsForm = '';
    private $_jsFormOnload = '';
    public function setTitle ($title)
    {
        $this->_title = $title;
    }
    public function setBaseUrl ($baseUrl)
    {
        $this->_baseUrl = $baseUrl;
    }
    public function setModel (ModelInterface $model)
    {
        $this->_model = $model;
    }

    /**
     *
     * @return \Core\ModelInterface
     */
    public function getModel ()
    {
        return $this->_model;
    }
    public function getBaseUrl ()
    {
        return $this->_baseUrl;
    }
    public function getTitle ()
    {
        return $this->_title;
    }
    public function index ()
    {
        $output = $_GET['output']??'html';
        $dataTable = $this->getDataTable();
        $data = [
            'title' => $this->_title,
            'baseURL' => $this->_baseUrl,
            'columns' => $this->_columns,
            'dataTable' => $dataTable
        ];
        if ($output == 'json') {
            return new ResponseSimple($dataTable);
        }

        return View::get('resource/index', $data);
    }
    
    public function show ($id)
    {
        $output = $_GET['output']??'html';
        $httpCode = 200;
        $model = $this->getModel();

        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        if ($output == 'json') {
            return new ResponseSimple($res->toArray(), $httpCode);
        }

        $data = [
            'title' => $this->_title,
            'baseURL' => $this->_baseUrl,
            'primaryKey' => $model->getPrimaryKey(),
            'id' => $id,
            'fields' => $this->getFieldsForm($id),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => $res->toArray()
        ];

        return View::get('resource/show', $data);
    }

    public function create ()
    {

        $model = $this->getModel();
        $data = [
            'title' => $this->_title,
            'baseURL' => $this->_baseUrl,
            'primaryKey' => $model->getPrimaryKey(),
            'id' => 0,
            'fields' => $this->getFieldsForm(0),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => []
        ];

        return View::get('resource/create', $data);
    }
    
    public function edit ($id)
    {
        $model = $this->getModel();
        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        $data = [
            'title' => $this->_title,
            'baseURL' => $this->_baseUrl,
            'primaryKey' => $model->getPrimaryKey(),
            'id' => $id,
            'fields' => $this->getFieldsForm($id),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => $res->toArray()
        ];
        return View::get('resource/edit', $data);
    }

    public function store ()
    {
        try {
            $model = $this->getModel();
            $fields = $model->getFields();
        
            $data = [];
            foreach ($fields as $field) {
                if ($field['name'] == $model->getPrimaryKey()) {
                    continue;
                }

                if (isset($_POST[$field['name']])) {
                    $data[$field['name']] = $_POST[$field['name']];
                } elseif (isset($_GET[$field['name']])) {
                    $data[$field['name']] = $_GET[$field['name']];
                }
            }
            if(!$this->validarForm($data)) {
                throw new \Exception("Preencha os campos corretamente");
            }
            $this->beforeSave($data);
            $status = true;
            if (!$id = $this->getModel()->insert($data)) {
                $id = null;
                $status = false;
            }
            
            $this->afterSave($data, $id);
            return ['id' => $id, 'status' => $status, 'message' => 'Salvo com sucesso'];
        } catch (\Exception $e) {
            return new ResponseSimple(['id' => null, 'status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function update ($id)
    {
        try {
            $model = $this->getModel();
            $fields = $model->getFields();
        
            $data = [];
            foreach ($fields as $field) {
                if ($field['name'] == $model->getPrimaryKey()) {
                    continue;
                }

                if (isset($_POST[$field['name']])) {
                    $data[$field['name']] = $_POST[$field['name']];
                } elseif (isset($_GET[$field['name']])) {
                    $data[$field['name']] = $_GET[$field['name']];
                }
            }
            if(!$this->validarForm($data, $id)) {
                throw new \Exception("Preencha os campos corretamente");
            }
            $this->beforeSave($data, $id);
            $this->getModel()->update($id, $data);
            $this->afterSave($data, $id);
            return ['id' => $id, 'status' => true, 'message' => 'Salvo com sucesso'];
        } catch (\Exception $e) {
            return new ResponseSimple(['id' => $id, 'status' => false, 'message' => $e->getMessage()], 500);
        }
    }
    public function delete ($id)
    {
        try {
            $this->getModel()->delete($id);
            return new ResponseSimple('', 200);
        } catch (\Exception $e) {
            return new ResponseSimple($e->getMessage(), 500);
        }
    }

    /**
     * Add as columns do datatable
     *
     * @param DataTable $dataTable
     * @return DataTable
     */
    public function setColumnDataTable ($dataTable)
    {
        $model = $this->getModel();
        $fields = $model->getFields();

        foreach ($fields as $field) {
            $table = $field['table'] ?? $model->getTable();
            $field['name'] = $field['name'];
            $this->addColumnDataTable(
                $dataTable,
                $field['name'],
                $table . '.' . $field['name'],
                StringCase::WordCapCase($field['name'])
            );
        }

        $this->addColumnAction($dataTable);

        return $dataTable;
    }

    public function addColumnAction ($dataTable)
    {
        $model = $this->getModel();

        $this->addColumnDataTable(
            $dataTable,
            'row_action',
            $model->getTable() . '.' . $model->getPrimaryKey(),
            '',
            false,
            false,
            function ($value, $dataRow) {
                $ret = '<a'.
                  ' href="'.$this->_baseUrl.'/'.$value.'/edit"'.
                  ' style="margin: 2px;"'.
                  ' class="btn btn-primary"'.
                  ' title="Editar"'.
                  '>'.
                  '<i class="fa fa-pencil"></i>'.
                  '</a> ';
                $ret .= ' <a'.
                  ' href="javascript::void(0)"'.
                  " onclick=\"deleteRecord('{$value}', '{$this->_baseUrl}')\"".
                  ' style="margin: 2px;"'.
                  ' class="btn btn-default"'.
                  ' title="Deletar"'.
                  '>'.
                  '<i class="fa fa-trash"></i>'.
                  '</a> ';
          
                return ''.$ret.'';
              },
              150
        );
        return $dataTable;
    }

    public function addColumnDataTable ($dataTable, $name, $nameModel = null, $label = null, $sortable = true, $searchable = true, $fncTransform = null, $width = null)
    {
        $dataTable->addColumn($name, $nameModel, $label, $sortable, $searchable, $fncTransform);
        $this->_columns[] = [
            'name' => $name,
            'nameModel' => $nameModel,
            'label' => $label,
            'sortable' => $sortable,
            'searchable' => $searchable,
            'fncTransform' => $fncTransform,
            'width' => $width
        ];
        return $dataTable;
    }

    public function setOrderDefaultDataTable ($dataTable)
    {
        $model = $this->getModel();
        $dataTable->setOrderDefault($model->getPrimaryKey(),  'asc');
        return $dataTable;
    }

    public function setJoinsDataTable ($dataTable)
    {
        $model = $this->getModel();
        // $dataTable->addJoin('table','id=id','LEFT');
        return $dataTable;
    }

    public function getDataTable ()
    {
      
      $model = $this->getModel();
      
      $dataTable = new DataTable();
  
      $dataTable->setModel($model);
      $this->setColumnDataTable($dataTable);  
      $this->setOrderDefaultDataTable($dataTable);
      $this->setJoinsDataTable($dataTable);
      
      $dataTable->addColumn('row_action',$model->getTable() .'.' . $model->getPrimaryKey(), null, false, false, function ($value, $dataRow) {
        $ret = '<a'.
          ' href="'.$this->_baseUrl.'/'.$value.'/edit"'.
          ' class="btn btn-primary"'.
          ' title="Editar"'.
          '>'.
          '<i class="fa fa-pencil"></i>'.
          '</a> ';
        $ret .= ' <a'.
            ' href="'.$this->_baseUrl.'/'.$value.'"'.
            ' class="btn btn-default"'.
            ' title="Ver"'.
            '>'.
            '<i class="fa fa-eye"></i>'.
            '</a> ';
        $ret .= ' <a'.
            ' href="javascript:void(0)"'.
            " onclick=\"deleteRecord('{$value}')\"".
            ' class="btn btn-default"'.
            ' title="Deletar"'.
            '>'.
            '<i class="fa fa-trash"></i>'.
            '</a> ';
  
        return $ret;
      });
  
      $dataTable->exec();
      
      return $dataTable->toArray();
    }

    public function getFieldsForm ($id)
    {
        $model = $this->getModel();

        return $model->getFields();

    }

    public function getJSForm ()
    {
        return $this->_jsForm;
    }
    public function getJSFormLoad ()
    {
        return $this->_jsFormOnload;
    }

    public function setEditJS ($js)
    {
        $this->_jsForm = $js;
    }
    public function setEditJSLoad ($js)
    {
        $this->_jsFormOnload = $js;
    }
    
    public function appendEditJS ($js)
    {
        $this->_jsForm .= $js;
    }
    public function appendEditJSLoad ($js)
    {
        $this->_jsFormOnload .= $js;
    }
    public function afterSave ($data, $id)
    {
        // Nada
    }
    public function beforeSave (&$data, $id)
    {
        // Nada
    }

    public function validarForm ($data, $id = null)
    {
        return true;
    }

}