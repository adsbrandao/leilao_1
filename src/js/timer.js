const leilaoTimer = {
    tempo: 30,
    play: false,
    timerInicio: 0,
    aoEncerrar: function () {
                
    },
    
    aoIniciar: function() {
        console.log('Mostrar início');
    },

    aoParar: function() {

    },

    
    //////// INICIAR CONTADOR

    iniciar: function (timerInicio = 0) {
        if (timerInicio == 0) {
            timerInicio = (new Date()).getTime();
        }

        this.timerInicio = timerInicio;
        this.play = true;
        this.render();
        //this.aoIniciar();
    },

    iniciarContador: function (idLeilao = 0) {
        fetch(baseURL + 'admin/leilao-iniciarcontador/' + idLeilao)
            .then(res => {
                console.log(res.json())
            });
    },

    //////// PARAR CONTADOR 
    
    parar: function () {
        this.play = false;
        this.timerInicio = 0;
        this.render();
       // this.aoParar();
    },

    pararContador: function (idLeilao = 0) {
        fetch(baseURL + 'admin/leilao-pararcontador/' + idLeilao)
            .then(res => {
                console.log(res.json())
            });
    },

    render: function () {
        console.log('play', this.play);
        if (this.play) {
            let timerAgora = (new Date()).getTime();
            let seconds = this.tempo - parseInt((timerAgora - this.timerInicio) / 1000);
            console.log(seconds);
            if (seconds < 1 ) {
                this.play = false;
                this.timerInicio = 0;
                $('#countdown').html(``);
                this.aoEncerrar();
                return true;
            }
            $('#countdown').html(`${seconds}`);
            this.aoIniciar();
            setTimeout(() => { this.render() }, 1000);
        } else {
            $('#countdown').html(``);
            this.aoParar();
        }
        
    }
};


// Quando usuario:
leilaoTimer.aoEncerrar = function () {
    console.log('Mostrar encerramento');
};

// leilaoTimer.aoIniciar = function () {
//     console.log('Mostrar inicio');
// };

// leilaoTimer.aoParar = function () {
//     console.log('Mostrar finalização');
// };

// AO clicar em iniciar
//leilaoTimer.iniciar();
