const leilaofoto = {
    _id: 0,
    _fotos: [],
    _selectorContainer: '.leilao-img-container',
    init: function (selectorContainer, id) {
        leilaofoto._selectorContainer = selectorContainer;
        leilaofoto._id = id;
        leilaofoto.limpar();
    },
    limpar: function () {
        fetch(baseURL + 'admin/leilao-foto/limpar')
            .then(res => {
                leilaofoto._fotos = [];
                leilaofoto.getFotos();
            })
    },
    getFotos: function () {
        fetch(baseURL + 'admin/leilao-foto/' + leilaofoto._id)
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    res.text().then(r => {
                        $(`${this._selectorContainer}`).html(r);
                    });

                    $(`${this._selectorContainer}`).html('Erro ao listar fotos');
                }
            })
            .then(res => {
                leilaofoto._fotos = res.data;
                leilaofoto.render();
            })
            .catch(reason => {
                $(`${this._selectorContainer}`).html('Erro ao listar fotos');
            })
    },

    addFoto: function () {
        $(`${this._selectorContainer} .fileinput`).click();
    },

    toJson: function () {
        let fotos = leilaofoto._fotos.map((foto) => {
            return Object.assign({deleted: false, ordem: 0}, foto);
        });

        return JSON.stringify(fotos);
    },

    uploadFoto: function () {
        let form = document.querySelector(`${this._selectorContainer} form.frm_leilaofoto`);
        let formData = new FormData(form);
        $(`${this._selectorContainer} .btn-add-foto`).html('<i class="fa fa-refresh fa-spin"></i> Adicionar foto');
        $(`${this._selectorContainer} .btn-add-foto`).prop('disabled', true);
        $(`${this._selectorContainer} .btn-add-foto`).addClass('disabled');
        http(
            baseURL + 'admin/leilao-foto/upload/' + leilaofoto._id,
            'POST',
            formData)
            .then(res => {

                $(`${this._selectorContainer} .btn-add-foto`).html('<i class="fa fa-plus"></i> Adicionar foto');
                $(`${this._selectorContainer} .btn-add-foto`).prop('disabled', false);
                $(`${this._selectorContainer} .btn-add-foto`).removeClass('disabled');
                if (res.ok) {
                    return res.json();
                } else {
                    res.text().then(r => {
                        $(`${this._selectorContainer} .msg`).html(r);
                    });

                    $(`${this._selectorContainer} .msg`).html('Erro no upload da foto');
                    // console.error(res.status);
                }
            })            
            .then(res => {
                leilaofoto._fotos = res;
                leilaofoto.render();
            })            
            .catch(reason => {
                $(`${this._selectorContainer} .msg`).html('Erro no upload da foto');
            })
    },

    deleteFoto: function (key) {
        if (Object.hasOwnProperty.call(leilaofoto._fotos, key)) {
            leilaofoto._fotos[key].deleted = true;
        }
        this.render();
    },
    setordem: function (key, dir) {
        if (Object.hasOwnProperty.call(leilaofoto._fotos, key)) {
            let ordem = leilaofoto._fotos[key].ordem ?? 0;
            if (dir == 'up') {
                leilaofoto._fotos[key].ordem = parseInt(ordem) - 1;
            } else {
                leilaofoto._fotos[key].ordem = parseInt(ordem) + 1;
            }
        }
        this.render();
    },
    render: function () {
        let html = '';

        let fotos = leilaofoto._fotos.filter((foto) => {
            return (!Object.hasOwnProperty.call(foto, 'deleted') || !foto.deleted);
        }).sort((a,b) => {
            if (a.ordem < b.ordem) {
                return -1;
            } else if (a.ordem > b.ordem) {
                return 1;
            } else {
                return 0;
            }
        });

        html += `<div>
            <button
                onclick="leilaofoto.addFoto();"
                type="button"
                class="btn btn-primary btn-add-foto"
                title=""><i class="fa fa-plus"></i> Adicionar foto</button>
            </div>`;

        html += `<div class="leilao-img-container">`;
        if (fotos.length == 0) {
            html += `<h3>Nenhuma foto</h3>`;
        }
        for (const foto of fotos) {
                html += `<div class="leilao-img thumb">`;
                // id: 3
                // imagem_original: "/upload/3.jpg"
                // imagem_thumb: "/upload/3.jpg"
                // leilao_id: 2
                // ordem: 0
                // path_original: ""
                // path_thumb: ""
                // console.log(foto);
                html += `<img src="${foto.imagem_thumb}" style="cursor: initial;" />`;
                html += `<div class="btns">`;
                html += `<button onclick="leilaofoto.deleteFoto(${foto.key})" class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button> `;
                html += `<button onclick="leilaofoto.setordem(${foto.key},'up')" class="btn btn-default" type="button"><i class="fa fa-chevron-left"></i></button> `;
                html += `<button onclick="leilaofoto.setordem(${foto.key},'down')" class="btn btn-default" type="button"><i class="fa fa-chevron-right"></i></button> `;
                html += `</div>`;
                html += `</div>`;
        }
        html += `<div class="clearfix"></div>`;
        html += `</div>`;

        html += `<div style="display: none;"><form class="frm_leilaofoto" method="post" enctype="multipart/form-data">
            <input type="file" name="arquivo[]" multiple class="fileinput" onchange="leilaofoto.uploadFoto();" />
            </form></div>`;

        $(`${this._selectorContainer}`).html(html);
    }
}