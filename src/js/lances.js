const lances = {
    _seletorContainer: '',
    _leilaoId: 0,
    _data: [],
    _msg: '',
    _msgCl: '',
    _timer: null,
    init: function (seletorContainer, leilaoId) {
        this._seletorContainer = seletorContainer;
        this._leilaoId = leilaoId;
        this.getData();
    },
    setMsg: function (message, msgCl = '') {
        this._msg = message;
        this._msgCl = msgCl;
        this.render();
    },
    fazerLance: function (parcelado, valor) {
        let that = this;
        let formData = new FormData();

        formData.append('parcelado', parcelado);
        formData.append('valor', valor);

        httpJSON(baseURL + `leilao-lances/${this._leilaoId}`, 'POST', formData)
            .then(res => {
                if (res.status) {
                    that.setMsg('', 'danger');
                    that._data = res.data;
                } else {
                    that.setMsg(res.message, 'danger');
                }
                this.render();
            })
            .catch(reason => {
                that.setMsg('Erro é ao buscar lances', 'danger');
                that._data = [];
            });
    },
    getData: function () {
        let that = this;
        if (that._timer !== null) {
            clearTimeout(that._timer);
            that._timer = null;
        }

        httpJSON(baseURL + `leilao-lances/${this._leilaoId}`)
            .then(res => {
                if (res.status) {
                    //that.setMsg('', 'danger');
                } else {
                    that.setMsg(res.message, 'danger');
                }

                // btn_lancar-parcel
                if (res.contador == null) {
                    leilaoTimer.parar();
                } else {
                    leilaoTimer.iniciar(res.contador);
                }
                if (res.parcelado == 0) {
                    $('#btn_lancar_parcela').removeClass('btn-success');
                    $('#btn_lancar_parcela').addClass('btn-default disabled');
                    $('#btn_lancar_parcela').prop('disabled', true);
                } else {
                    $('#btn_lancar_parcela').addClass('btn-success');
                    $('#btn_lancar_parcela').removeClass('btn-default disabled');
                    $('#btn_lancar_parcela').prop('disabled', false);
                }
                that._data = res.data;
                that.render();
                that._timer = setTimeout(() => that.getData(), 2000);
            })
            .catch(reason => {
                console.log(reason);
                that.setMsg('Erro ao buscar lances', 'danger');
                that._data = [];
            });
    },
    render: function () {
        let html = '';

        if (this._data.length == 0) {
            html = `<div class="text-${this._msgCl}">${this._msg}</div>`;
        } else {
            html += `<table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="1">Usuário</th>
                        <th width="1">Data</th>
                        <th width="1">Valor</th>
                        <th width="1">Parcelas</th>
                    </tr>`;
            if (this._msg != '') {
                html += `<tr>
                        <th colspan="4"><span class="text-${this._msgCl}">${this._msg}</span></th>
                    </tr>`;
            }
            html += `</thead><tbody>`;

            let first = true;
                
            for (const lance of this._data) {
                let date = new Date(lance.data_create);
                let valor = Number(lance.valor).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL'});
                let parcelado = Number(lance.parcelado)==0 ? 'À vista':'Parcelado' ;
                
                if (first) {
                    html += `<tr class="success">`;
                    first = false;
                } else {
                    html += `<tr>`;
                }
                html += `<td>` + String(lance.nome).substr(0, 20) + `</td>`;
                html += `<td>${date.toLocaleString()}</td>`;
                html += `<td style="text-align: right;"><nobr>${valor}</nobr></td>`;
                html += `<td>${parcelado}</td>`;
                html += `</tr>`;
            }

            html += `</tbody></table>`;
        }

        $(this._seletorContainer).html(html);
    }
};