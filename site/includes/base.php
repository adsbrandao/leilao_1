<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="autor" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=\URL;?>site/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=URL?>site/js/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=URL?>site/js/owlcarousel/assets/owl.theme.default.min.css"> 
    <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <title>Norte Leilões</title>
    <link rel="stylesheet" href="<?=\URL;?>site/css/newcss3.css">
    <style>
       .cont5 {
            /* margin: 0 auto; */
            display: flex;
           
        }
        .item_nav {
            /* O flex: 1; é necessário para que cada item se expanda ocupando o tamanho máximo do container. */
            /* flex: 1; */
            
            padding: auto;
        }
        .center {
          justify-content: center;
        }
    </style>
        <script src="<?=URL?>site/js/jquery.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="<?=URL?>site/js/owlcarousel/owl.carousel.js"></script>
    <script src="<?=URL?>site/js/owlcarousel/owl.carousel.min.js"></script>
</head>
<body class="tipo">
<?php 

include 'header.php';

echo $obContent ?? '';
include 'footer.php';

?>
    <!-- <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->


</body>
</html>
<?php exit;?>

