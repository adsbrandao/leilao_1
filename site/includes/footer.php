<footer style="background-color: rgb(64, 64, 64);">
  <!-- rodape 1 -->
  <section class="background-color: rgb(64, 64, 64);">
    <section class="artigos cont1 center" style="background-color: rgb(64, 64, 64); ">
      <article class="article_format caixa_div_outras" style="background-color: rgb(64, 64, 64);">
        <center>
          <div class="item" style="">
            <h4 style="margin-bottom:-2px;margin-top: 0px!important;color:#fff;">A GENTE CONECTADO POR E-MAIL</h4>
          </div>
        </center>
      </article>
    </section>
  </section>
  <!-- fim rodape 1 -->
  <!-- rodape 2 -->
  <section style="background-color: rgb(64, 64, 64);">
    <section class="artigos cont1 center" style="background-color: rgb(64, 64, 64); z-index:-2;">
      <article class="article_format caixa_div_outras" style="background-color: rgb(64, 64, 64);color: #fff;">
        <div class="item_footer" style="">
          <div style="min-width:200px!important;">
          <p style="">Receba em seu e-mail todos os leilões e <br> informações dos bens com exclusividade</p>
          </div>
        </div>
      </article>
      <article class="article_format caixa_div_outras" style="background-color: rgb(64, 64, 64);color: #fff;">
        <div class="item" style="">
          <form id="frm_pesquisa" action="<?=URL?>leilao_procura">
            <input type="hidden" name="p" id="f_page" value="<?= $page ?? 0 ?>">
            <div class="pull-left">
              <input type="text" name="q" value="<?= $q ?? '' ?>" class="form-control" onchange="$('#frm_pesquisa').submit();" placeholder="Pesquisa">
            </div>
            <div class="pull-left">
              <button type="submit" class="btn " style="background-color: rgb(230, 230, 230);"><i class="fa fa-search"></i></button>
            </div>
          </form>
          <!-- <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <span class="input-group-text" id="basic-addon2">@example.com</span>
              </div> -->
        </div>
      </article>
    </section>
  </section>
  <!-- fim rodape 2 -->
  <!-- rodape 3 -->
  <section style="background-color:Rgb(180,0,4);">
    <section class="artigos cont1 center" style="background-color:Rgb(180,0,4); z-index:-2;padding:40px 0px;">
      <article class="article_format caixa_div_outras" style="background-color:Rgb(180,0,4);color: #fff;">
        <div class="" style="text-align:left;">
          <div class="module">
            <ul class="flex-start" style="">
              <li style="">
                <h6>NORTE DE MINAS LEILÕES</h6>
              </li>
              <li style="">Home</li>
              <li style="">Serviços</li>
              <li style="">Quem somos</li>
              <li style="">Quem somos</li>
              <li style="">Quem somos</li>
              <li style="">Contato</li>
            </ul>
          </div>

        </div>
      </article>
      <article class="article_format caixa_div_outras" style="background-color:Rgb(180,0,4);color: #fff;">
        <div class="item" style="">
          <div class="module">
            <ul class="flex-start" style="">
              <li style="margin-top:-10px;">
                <h6>ATENDIMENTOS</h6>
              </li>
              <li style="">email@gmail.com</li>
              <li style="">(31)99999-9999</li>
            </ul>
          </div>

        </div>
        <!-- <p style="margin-top: 0px!important;">IMOVEL A <br> LEILAO 131415 </p> -->
        </div>
      </article>
      <article class="article_format caixa_div_outras" style="background-color:Rgb(180,0,4);color: #fff;">
        <section class="artigos_c cont1">
          <article class="article_format_b " style="margin-top:-3px;">
            <h6>SIGA-NOS</h6>
          </article>
        </section>
        <section class="artigos_c cont1" style="margin-top:-20px;">
          <article class="article_format_b "><i class="fab fa-facebook-f"></i></article>
          <article class="article_format_b "><i class="fab fa-instagram"></i></article>
        </section>
        <!-- <div class="item" style="">
                <div class="module">
                <ul class="flex-start" style="">
                  <li style="margin-top:-10px;"><h6>ATENDIMENTOS</h6></li>
                  <li style=""><i class="	fa fa-facebook"></i></li>
                  <li style=""><i class="	fa fa-instagram"></i></li>
                </ul>
                </div>
              </div> -->
        <!--  <li style=""><i class="	fa fa-facebook"></i></li>
                  <li style=""><i class="	fa fa-instagram"></i></li> -->
      </article>
    </section>
  </section>


  <!-- fim rodape 3 -->
</footer>