<?php
use Models\Leilao;
?>
<main style="position: relative;">
<section class="artigos cont1 center" style="background-color: rgb(255, 255, 255); z-index:-2;">
          <?php
          $leiloes = Leilao::findAll("status > 0 and nome like ? or lote like ? ", [
              '%'.($_GET['q']??'').'%',
              '%'.($_GET['q']??'').'%'
          ]);
          while ($leilao = $leiloes->fetch()) {
          ?>
            <article class="article_format_b caixa_div_outras">
              <a href="<?= URL ?>leilao/<?= $leilao->id; ?>">
                <div class="item_thumbnail" style="border:1px solid rgb(206, 206, 206);">
                  <?php
                  if (is_null($leilao->imagem_thumb) || !is_file(\PATH . 'upload/' . $leilao->imagem_thumb)) {
                    echo '<img src="' . \URL . 'theme/img/logo.png" style="opacity:0.3;width: 100%;max-height: 200px;" />';
                  } else {
                    echo '<img src="' . \URL . 'upload/' . $leilao->imagem_thumb . '" style="width: 100%;max-height: 200px;" />';

                  ?>
                    <img src="<?= URL ?>site/img/exemplo2.jpg">
                  <?php
                  }
                  ?>
                  <p style="margin-bottom: -0px!important;text-align: left;color:black;">Leilao: <?= $leilao->nome; ?></p>
                  <article class="cont1 row1">
                    <div class="item_button ">
                      <p style="border:0px solid black;font-size: 14px;color:black;">Lote: <?= $leilao->lote; ?></p>
                    </div>
                    <div class="item_button " style="margin-left: 10px;">
                      <p style="border:0px solid black;font-size: 14px;color:black;"><?= $leilao->status; ?></p>
                    </div>
                  </article>
                  <p style="margin-bottom: -0px!important;text-align: left;color:black;">Status: <?= $leilao->valor_inicial; ?></p>
                  <a href="<?= URL ?>leilao/<?= $leilao->id; ?>" class="" style="    border: 0px solid black;
                                          font-size: 14px;
                                          background-color: rgb(51, 204, 51);padding: 5px 5px;
                                          color: #fff;">Clique aqui</a>
                </div>
              </a>
            </article>
          <?php } // Fim do while do leilao 
          ?>
        </section>
</main>