<?php 
use Models\Leilao;
use Models\Lance;
use Models\LeilaoFoto;
$leilao = Leilao::find($id);

// echo $leilao->processo;
?> 
                   
<main class="main item_leilao" style="position: relative;">
          <section class="gestao-espacamento" style="z-index: -2;">
            <div class="sessao-principal" style="">
                <!-- começo section -->
                <section style="background-color: rgb(255, 255, 255);">
                <!-- padding-bottom: 40px;background-color: rgb(51, 133, 255); -->
                
                <div class="artigos cont1 " style="background-color: rgb(255, 255, 255); z-index:-2;">
                  <article class="article_format caixa_div_outras" style="">
                    <div class="item" style="border:1px solid rgb(206, 206, 206);">
                      <div class="artigos_b cont1 ">
                        <article class="article_format">
                          <?php 
                            $fotos = LeilaoFoto::findAll(['leilao_id' => $id], [], 'ordem ASC');
                            while($foto = $fotos->fetch()){

                          ?>
                          <img src="<?=URL;?>upload/<?=$foto->imagem_original?>" alt="" style="width: 400px;">
                        <?php
                          }
                        ?>
                        </article>
                        <article class="article_format_b">
                          <div class="artigos_c cont1">
                            <table class="">
                              <thead>
                                <tr>
                                  <th scope="">Lote</th>
                                  <th scope=""><?=$leilao->lote;?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="">Encerramento</th>
                                  <td><?=$leilao->dateTimeFormat('data_fim');?></td>
                                </tr>
                                <tr>
                                  <th scope="">Valor Inicial</th>
                                  <!-- $leilao->valor_inicial; -->
                                  <td>R$<?=number_format($leilao->valor_inicial, 2, ',','.')?></td>
                                </tr>
                                <tr>
                                  <th scope="">
                                    Incremento
                                  </th>
                                  <td><?=$leilao->salto_lance;?></td>
                                </tr>
                                <tr>
                                  <th scope="">Arrematado por </th>
                                  <td><?=$leilao->arrematado_por;?></td>
                                </tr>
                                <tr>
                                  <th scope="">Valor Arrematado</th>
                                  <td><?=$leilao->valor_arrematado;?></td>
                                </tr>
                                <tr>
                                  <th scope="">Processo</th>
                                  <td><?=$leilao->processo;?></td>
                                </tr>
                                <tr>
                                  <th scope="">Descrição</th>
                                  <td><?=$leilao->descricao;?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="artigos_c cont1">
                            <table class="">
                              <thead>
                                <tr>
                                  <th scope="" class="definicao border border-white bg-secondary text-white" style="">Usuario</th>
                                  <th scope="" class="td_defini border border-white bg-secondary text-white">Data</th>
                                  <th scope="" class="td_defini border border-white bg-secondary text-white">Valor</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php 
                                $lances= Lance::findAll([
                                    "leilao_id"=>$id
                                ]);

                                while($lance=$lances->fetch()){
                                     /**
    * @var mixed $id;
    * @var mixed $data_create;
    * @var mixed $usuario_id;
    * @var mixed $cliente_id;
    * @var mixed $leilao_id;
    * @var mixed $valor; */
                              ?>
                                <tr>
                                  <th scope="" class="definicao border border-secondary"  style=""><?=$lance->nome;?></th>
                                  <td class="td_defini border border-secondary" style=""><?=$lance->data_create;?></td>
                                  <td class="td_defini border border-secondary" style="">R$<?=$lance->valor;?></td>
                                </tr>
                               <?php
                               }
                               ?>
                              </tbody>
                            </table>
                          </div>
                        </article>
                      </div>
                   
                    </div>
                </article> 
                </div>
                <!-- fim div -->
            </div>
             </div>
          </section>
    </main>
    <?php 
// $leilao->id
// $leilao->status ***(caixa azul)
  // status 0 = NAO_PUBLICADO
  // status 1 = PUBLICADO
  // status 2 = ANDAMENTO
  // status 3 = ENCERRADO
  // status 4 = CANCELADO
  // $leilao->data_fim ***
  // $leilao->data_inicio ***
  // $leilao->lote *** (numero do leilao)
  // $leilao->nome ***
// $leilao->data_create
// $leilao->data_update
// $leilao->valor_inicial ***
// $leilao->usuario_inicio
// $leilao->usuario_fim
// $leilao->arrematado_por
// $leilao->categoria_id
// $leilao->processo 
// $leilao->descricao
// $leilao->salto_lance
// $leilao->valor_arrematado
// $leilao->destaque
// $leilao->parcelado 
// $leilao->contador
?>