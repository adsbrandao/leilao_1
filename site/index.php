<?php

use Models\Leilao;
?>
<!-- leilao/leilao/1 -->
<main style="position: relative;">
  <section class="gestao-espacamento">
    <div class="sessao-principal">
      <!-- começo section 
              usuario@leilao.com
              123456
              -->
      <section style="background-color: rgb(255, 255, 255);">
        <!-- padding-bottom: 40px;background-color: rgb(51, 133, 255); -->
        <section class="artigos cont1 center" style="background-color: rgb(245, 245, 245); z-index:-2;">
          <article class="article_format caixa_div_outras">
            <div class="item item_icon " style="">
              <i class="	fa fa-building"></i>
              <p class="item_icon">Imoveis</p>
            </div>
          </article>
          <article class="article_format caixa_div_outras">
            <div class="item item_icon" style="">
              <i class="	fa fa-cogs"></i>
              <p class="item_icon">Automóveis</p>
            </div>
          </article>
          <article class="article_format caixa_div_outras">
            <div class="item item_icon" style="">
              <i class="fa fa-wrench"></i>
              <p class="item_icon">Máquinas</p>
            </div>
          </article>
          <article class="article_format caixa_div_outras">
            <div class="item item_icon" style="">
              <div class="module">
                <i class="fa fa-motorcycle"></i>
                <p class="item_icon">Equipamentos</p>
              </div>
          </article>
          <article class="article_format caixa_div_outras">
            <div class="item item_icon" style="">
              <i class="fa fa-bus"></i>
              <p class="item_icon">Onibus</p>
            </div>
          </article>
          <article class="article_format caixa_div_outras">
            <div class="item item_icon" style="">
              <i class="fa fa-truck"></i>
              <p class="item_icon">Caminhoes</p>
            </div>
          </article>
        </section>
        <section class="artigos cont1 flex-start">
          <div class="artigos_c cont1 flex-start">
            <article class="article_format_b" style="padding: 0px 0px;margin-bottom: 0px;">
              <H6 style="color:rgb(255, 153, 0);">Destaque</H6>
            </article>
          </div>
          <div class="artigos_c cont1 flex-start">
            <div style="background-color: rgb(166, 166, 166);width: 100%;height: 1px;">
              <div style="background-color: rgb(255, 153, 0);width: 80px;height: 1px;"></div>
            </div>
          </div>
        </section>
        <section>
          <div class="owl-carousel owl-theme owl-loaded" style="margin:auto;width:60%;">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="">
                    <p class="arti_little">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="">
                    <p class="arti_little">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
                <div class="owl-item">
                  <div class="article_format_b destaque">
                    <img class="img_arti" src="site/img/clara_logo_carro_exemplo.jpg" alt="" style="">
                    <p class="arti_little" style="">Texto</p>
                    <div class="article_format_box arti">
                      <p style="font-size:10px;">HOTEL - CENTRO BH</p>
                      <p style="font-size:10px;">Leilao xxxxx/xx</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- 1 section Leilao -->
        <section class="artigos cont1 flex-start">
          <div class="artigos_c cont1 flex-start">
            <article class="article_format_b" style="padding: 0px 0px;margin-bottom: 0px;">
              <H6 style="color:rgb(255, 153, 0);">Leilões</H6>
            </article>
          </div>
          <div class="artigos_c cont1 flex-start">
            <div style="background-color: rgb(166, 166, 166);width: 100%;height: 1px;">
              <div style="background-color: rgb(255, 153, 0);width: 80px;height: 1px;"></div>
            </div>
          </div>
        </section>
        <section class="container-fluid">
          <div class="row justify-content-md-center">
            <?php
            $leiloes = Leilao::findAll("status > 0");
            while ($leilao = $leiloes->fetch()) {
            ?>
              <article class="col-lg-2 col-md-2 col-sm-12" style="border: 1px solid rgb(206, 206, 206);">
                <a href="<?= URL ?>leilao/<?= $leilao->id; ?>">
                  <?php
                  if (is_null($leilao->imagem_thumb) || !is_file(\PATH . 'upload/' . $leilao->imagem_thumb)) {
                    echo '<img src="' . \URL . 'theme/img/logo.png" style="opacity:0.3;width: 100%;max-height: 200px;" />';
                  } else {
                    echo '<img src="' . \URL . 'upload/' . $leilao->imagem_thumb . '" style="width: 100%;max-height: 200px;" />';

                  ?>
                    <img class="img_block" src="img/exemplo2.jpg" alt="" style="">
                  <?php
                  }

                  ?>
                  <p class="breakdown_p" style="margin-bottom: -0px!important;text-align: left;">Leilao:
                    <?= $leilao->nome; ?>
                  </p>
                  <!-- dados do leilao -->
                  <div class="caixa_linha_flex altura_espaçamento" style="">
                    <div class="caixa_flex breakdown_p" style="height: 10px;">
                      <p style="">Leilao:
                        <?= $leilao->nome; ?>
                      </p>
                    </div>
                    <div class="caixa_flex breakdown_p" style="height: 10px;">
                      <p style="">Lote:
                        <?= $leilao->lote; ?>
                      </p>
                    </div>
                  </div>
                  <p class="breakdown_p" style="margin-bottom: -0px!important;text-align: left;">Valor:
                    <?= $leilao->valor_inicial; ?>
                  </p>
                  <button class="btn btn-success btn-block" style="margin-bottom: 20px;">Clique aqui</button>
                  <!-- fim dados -->
                </a>
              </article>
            <?php
            }
            ?>
          </div>
        </section>

        <!-- fim section -->
      </section>
    </div>
  </section>
</main>
<script>
  $(document).ready(function() {
    $('.owl-carousel').owlCarousel({
      loop: false,
      margin: 10,
      nav: true,
      autoWidth: true,
      dots: false,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    })
  });
</script>