<?php namespace App\Controllers\Admin;

use Core\Controller;
use Core\View;

class ConfiguracaoController extends Controller {

    public function index () {
        return View::get('admin/config');
    }

    public function pagamento () {
        return View::get('admin/config_pagamento');
    }
}
