<?php namespace App\Controllers\Admin;

use Core\ControllerResource;
use Models\Usuario;

class UsuarioController extends ControllerResource {

    public function __construct () {
        $this->setTitle('Usuários');
        $this->setBaseUrl(URL.'admin/usuario');
        $this->setModel(new Usuario());
    }
    public function setColumnDataTable ($dataTable)
    {

        $this->addColumnAction($dataTable);


        $model = $this->getModel();
        $fields = $model->getFields();
        $table = $model->getTable();

        $this->addColumnDataTable(
            $dataTable,
            'id',
            $table . '.id',
            'ID',true, true,null, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'status',
            $table . '.status',
            'Status',true, true,function ($v, $rowData) {
                if($v == 0) {
                    return 'Inativo';
                } elseif($v == 1) {
                    return 'Ativo';
                }
                return 'Inativo';
            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'grupo_id',
            $table . '.grupo_id',
            'Grupo',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                }
                $res = \Models\Grupo::find($v);
                if(!$res) {
                    return '-';
                } elseif($res->status == 0) {
                    return $res->nome.' (Inativo)';
                } else {
                    return $res->nome;
                }
            }, 1
        );
        

        $this->addColumnDataTable(
            $dataTable,
            'nome',
            $table . '.nome',
            'Nome'
        );

        $this->addColumnDataTable(
            $dataTable,
            'telefone',
            $table . '.telefone',
            'Telefone',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'celular',
            $table . '.celular',
            'Celular',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'email',
            $table . '.email',
            'E-mail',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'data_update',
            $table . '.data_update',
            'Última alteração',
            true, true,
            function ($value, $dataRow) {
                $value = strtotime($value);
                if (!$value || is_null($value) || $value < 100) {
                    $value = '-';
                } else {
                    $value = date('d/m/Y H:i:s', $value);
                }
                return '<nobr>'.$value.'</nobr>';
            },
            1
        );

        return $dataTable;
    }

    public function getFieldsForm ($id)
    {
        $model = $this->getModel();

        $i = 0;

        $fieldsModel = $model->getFields();
        $fields = [];

        $fields[$i++] = $fieldsModel['id'];

        // ---------------------------
        $fields[$i] = $fieldsModel['status'];
        $fields[$i]['label'] = 'Status';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Inativo'],
            ['value' => 1, 'label' => 'Ativo']
        ];
        $fields[$i]['default'] = 1;
        // $fields[$i]['maxlen'] = 255;
        // $fields[$i]['precision'] = 0;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['grupo_id'];
        $fields[$i]['label'] = 'Grupo';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [];
        $fields[$i]['default'] = null;

        $resGrupos = \Models\Grupo::findAll(['status'=> 1], [], 'nome ASC');
        while($grupo = $resGrupos->fetch()) {
            $fields[$i]['options'][] = ['value' => $grupo->id, 'label' => $grupo->nome];
        }

        // $fields[$i]['maxlen'] = 255;
        // $fields[$i]['precision'] = 0;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['nome'];
        $fields[$i]['label'] = 'Nome';
        $fields[$i]['classCol'] = 'col-md-8 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        // ---------------------------
        $fields[$i] = ['name' => 'separador'];
        $fields[$i]['label'] = 'Contato e Login';
        $fields[$i]['classCol'] = 'clearfix';
        $fields[$i]['native_type'] = 'SEPARADOR';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['telefone'];
        $fields[$i]['label'] = 'Telefone';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'TELEFONE';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['celular'];
        $fields[$i]['label'] = 'Celular';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'CELULAR';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['email'];
        $fields[$i]['label'] = 'Email';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'EMAIL';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['senha'];
        $fields[$i]['label'] = 'Senha';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'PASSWORD';
        $fields[$i]['default'] = '';
        $i++;

        // "data_create": "2021-03-05 11:01:15",
        // "data_update": "2021-03-05 11:01:15",
        // "data_acesso": "2021-03-05 11:01:15",
        // "imagem": null,

        return $fields;

    }
    public function beforeSave (&$data, $id = null)
    {
        if (is_null($id)) {
            $data['data_create'] = date('Y-m-d H:i:s', time());
            $data['data_acesso'] = date('Y-m-d H:i:s', time());
        }
        $data['data_update'] = date('Y-m-d H:i:s', time());
        
        if ($data['senha'] == '') {
            unset($data['senha']);
        } else {
            $data['senha'] = md5($data['senha']);
        }
    }

    public function validarForm ($data, $id = null)
    {
        if (is_null($id)) {
            if (!isset($data['senha']) || $data['senha'] == '') {
                throw new \Exception("Preencha a senha");
            }
        }

        if (!isset($data['email']) || $data['email'] == '') {
            throw new \Exception("Email é campo obrigatório");
        }

        return true;
    }
    // public function index () {}
    // public function show ($id) {}
    // public function create () {}
    // public function edit ($id) {}
    // public function store () {}
    // public function update ($id) {}
    // public function delete ($id)
}
