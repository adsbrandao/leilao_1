<?php namespace App\Controllers\Admin;

use Core\ControllerResource;
use Core\ValidarCpfCnpj;
use Core\View;
use Models\Cliente;

class ClienteController extends ControllerResource {

    public function __construct () {
        $this->setTitle('Clientes');
        $this->setBaseUrl(URL.'admin/cliente');
        $this->setModel (new Cliente());
    }
    public function setColumnDataTable ($dataTable)
    {

        $this->addColumnAction($dataTable);

        $model = $this->getModel();
        $fields = $model->getFields();
        $table = $model->getTable();

        $this->addColumnDataTable(
            $dataTable,
            'id',
            $table . '.id',
            'ID',true, true,null, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'status',
            $table . '.status',
            'Status',true, true,function ($v, $rowData) {
                if($v == 0) {
                    return 'Inativo';
                } elseif($v == 1) {
                    return 'Ativo';
                } elseif($v == 2) {
                return 'Novo';
            }
                return 'Inativo';
            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'nome',
            $table . '.nome',
            'Nome'
        );

        $this->addColumnDataTable(
            $dataTable,
            'cpf',
            $table . '.cpf',
            'CPF',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'cnpj',
            $table . '.cnpj',
            'CNPJ',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'telefone',
            $table . '.telefone',
            'Telefone',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'celular',
            $table . '.celular',
            'Celular',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'email',
            $table . '.email',
            'E-mail',true, true,
            function ($v) {
                return '<nobr>'.$v.'</nobr>';

            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'data_update',
            $table . '.data_update',
            'Última alteração',
            true, true,
            function ($value, $dataRow) {
                $value = strtotime($value);
                if (!$value || is_null($value) || $value < 100) {
                    $value = '-';
                } else {
                    $value = date('d/m/Y H:i:s', $value);
                }
                return '<nobr>'.$value.'</nobr>';
            },
            1
        );

        return $dataTable;
    }

    public function getFieldsForm ($id)
    {
        $model = $this->getModel();

        $i = 0;

        $fieldsModel = $model->getFields();
        $fields = [];

        $fields[$i++] = $fieldsModel['id'];

        // ---------------------------
        $fields[$i] = $fieldsModel['status'];
        $fields[$i]['label'] = 'Status';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Inativo'],
            ['value' => 1, 'label' => 'Ativo'],
            ['value' => 2, 'label' => 'Novo']
        ];
        $fields[$i]['default'] = 2;
        // $fields[$i]['maxlen'] = 255;
        // $fields[$i]['precision'] = 0;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['nome'];
        $fields[$i]['label'] = 'Nome';
        $fields[$i]['classCol'] = 'col-md-4 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['sobrenome'];
        $fields[$i]['label'] = 'Sobrenome';
        $fields[$i]['classCol'] = 'col-md-4 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 255;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['nascimento'];
        $fields[$i]['label'] = 'Nascimento';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'DATE';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = ['name' => 'tipo_pessoa'];
        $fields[$i]['label'] = 'Tipo pessoa';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['default'] = 0;
        $fields[$i]['onchange'] = 'showTipoPessoa(this.value)';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Física'],
            ['value' => 1, 'label' => 'Jurídica']
        ];
        $i++;

        $this->appendEditJS("const testarCadastro = function () {
            let email = $('#f_email').val();
            let cpf = $('#f_cpf').val();
            let cnpj = $('#f_cnpj').val();
            let tipoPessoa = $('#f_tipo_pessoa').val();
            if (tipoPessoa == 0 || tipoPessoa == '0') {
                cnpj = '';
            } else {
                cpf = '';
            }

            testeCadastro(
                {$id},
                email,
                cpf,
                cnpj,
                (msg) => {                
                    showMsg('#msg_salvar', '');
                    $('#btn_salvar')
                        .removeClass('disabled')
                        .prop('disabled', false)
                        .html('Cadastrar');
                },
                (msg) => {
                    showMsg('#msg_salvar', msg, 'fa fa-times', 'danger');
                    $('#btn_salvar')
                        .addClass('disabled')
                        .prop('disabled', true)
                        .html('Cadastrar');
                });
        };");
        $this->appendEditJS("const showTipoPessoa = (value) => {
            if (value == 1 || value == '1') {
                $('.pessoa-fisica').hide();
                $('.pessoa-juridica').show();
            } else {
                $('.pessoa-juridica').hide();
                $('.pessoa-fisica').show();
            }
        };");

        $this->appendEditJSLoad("
        // let cpf = $('#f_cpf').val();
        let cnpj = $('#f_cnpj').val();
        if (cnpj == '') {
            $('#f_tipo_pessoa').val('0');
            showTipoPessoa(0);
        } else {
            $('#f_tipo_pessoa').val('1');
            showTipoPessoa(1);
        }
        $('#f_email').change(testarCadastro);
        $('#f_cpf').change(function () {
            if(testCPF($(this).val())) {
                testarCadastro();
            } else {
                showMsg('#msg_salvar', 'CPF Inválido', 'fa fa-times', 'danger');
            }
        });
        $('#f_cnpj').change(function () {
            if(testCNPJ($(this).val())) {
                testarCadastro();
            } else {
                showMsg('#msg_salvar', 'CNPJ Inválido', 'fa fa-times', 'danger');
            }
        });
        ");

        // ---------------------------
        $fields[$i] = $fieldsModel['cpf'];
        $fields[$i]['label'] = 'CPF';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12 pessoa-fisica';
        $fields[$i]['native_type'] = 'CPF';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 14;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['razao_social'];
        $fields[$i]['label'] = 'Razão social';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12 pessoa-juridica';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 255;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['cnpj'];
        $fields[$i]['label'] = 'CNPJ';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12 pessoa-juridica';
        $fields[$i]['native_type'] = 'CNPJ';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 18;
        $i++;

        // ---------------------------
        $fields[$i] = ['name' => 'separador'];
        $fields[$i]['label'] = 'Contato e Login';
        $fields[$i]['classCol'] = 'clearfix';
        $fields[$i]['native_type'] = 'SEPARADOR';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['telefone'];
        $fields[$i]['label'] = 'Telefone';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'TELEFONE';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['celular'];
        $fields[$i]['label'] = 'Celular';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'CELULAR';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['email'];
        $fields[$i]['label'] = 'Email';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'EMAIL';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['senha'];
        $fields[$i]['label'] = 'Senha';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'PASSWORD';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = ['name' => 'separador'];
        $fields[$i]['label'] = 'Endereço';
        $fields[$i]['classCol'] = 'clearfix';
        $fields[$i]['native_type'] = 'SEPARADOR';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['cep'];
        $fields[$i]['label'] = 'CEP';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'CEP';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['endereco'];
        $fields[$i]['label'] = 'Endereço';
        $fields[$i]['classCol'] = 'col-md-6 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 255;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['numero'];
        $fields[$i]['label'] = 'Numero';
        $fields[$i]['classCol'] = 'col-md-4 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 50;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['complemento'];
        $fields[$i]['label'] = 'Complemento';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 50;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['bairro'];
        $fields[$i]['label'] = 'Bairro';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 50;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['cidade'];
        $fields[$i]['label'] = 'Cidade';
        $fields[$i]['classCol'] = 'col-md-4 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 255;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['estado'];
        $fields[$i]['label'] = 'UF';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 2;
        $i++;
        // "data_create": "2021-03-05 11:01:15",
        // "data_update": "2021-03-05 11:01:15",
        // "data_acesso": "2021-03-05 11:01:15",
        // "imagem": null,
        // "documento_frete": null,
        // "documento_verso": null

        return $fields;

    }

    public function create ()
    {

        $model = $this->getModel();
        $data = [
            'title' => 'Clientes',
            'baseURL' => \URL.'admin/cliente',
            'primaryKey' => $model->getPrimaryKey(),
            'id' => 0,
            'fields' => $this->getFieldsForm(0),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => []
        ];

        return View::get('admin/cliente_edit', $data);
    }
    
    public function edit ($id)
    {
        $model = $this->getModel();
        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        $data = [
            'title' => 'Clientes',
            'baseURL' => \URL.'admin/cliente',
            'primaryKey' => $model->getPrimaryKey(),
            'id' => $id,
            'fields' => $this->getFieldsForm($id),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => $res->toArray()
        ];
        return View::get('admin/cliente_edit', $data);
    }

    public function beforeSave (&$data, $id = null)
    {
        if (is_null($id)) {
            $data['data_create'] = date('Y-m-d H:i:s', time());
            $data['data_acesso'] = date('Y-m-d H:i:s', time());
        }
        $data['data_update'] = date('Y-m-d H:i:s', time());
        $tipoPessoa = $_POST['tipo_pessoa']?? -1;

        if ($tipoPessoa == -1) {
            if ($data['cnpj']=='') {
                $tipoPessoa = 1;
            } else {
                $tipoPessoa = 0;
            }
        }

        if ($tipoPessoa == 0) {
            $data['cnpj'] = '';
            $data['razao_social'] = '';

            if (!isset($data['cpf']) || $data['cpf'] == '') {
                throw new \Exception("CPF é campo obrigatório para pessoa física");
            }

            if (!ValidarCpfCnpj::testCPF($data['cpf'])) {
                throw new \Exception("CPF inválido");
            }
        } else {
            $data['cpf'] = '';

            if (!isset($data['razao_social']) || $data['razao_social'] == '') {
                throw new \Exception("Razão social é campo obrigatório para pessoa jurídica");
            }

            if (!isset($data['cnpj']) || $data['cnpj'] == '') {
                throw new \Exception("CNPJ é campo obrigatório para pessoa jurídica");
            }

            if (!ValidarCpfCnpj::testCNPJ($data['cnpj'])) {
                throw new \Exception("CNPJ inválido");
            }
        }

        if ($data['senha'] == '') {
            unset($data['senha']);
        } else {
            $data['senha'] = md5($data['senha']);
        }
    }

    public function validarForm ($data, $id = null)
    {
        if (is_null($id)) {
            if (!isset($data['senha']) || $data['senha'] == '') {
                throw new \Exception("Preencha a senha");
            }
        }

        if (!isset($data['email']) || $data['email'] == '') {
            throw new \Exception("Email é campo obrigatório");
        }

        return true;
    }

    public function getClientesAtivos ()
    {
        try {
            $q = $_GET['q']??'';
            $page = intval($_GET['page']??0);
            $resultsPerPage = 20;
            $start = $page * $resultsPerPage;

            $where = '';
            $bindData = [];

            if ($q != '') {
                $where = " AND (";
                $where .= " id = ?";
                $bindData[] = intval($q);

                $where .= " OR nome LIKE ?";
                $bindData[] = '%'.$q.'%';

                $where .= " OR sobrenome LIKE ?";
                $bindData[] = '%'.$q.'%';

                $where .= " OR razao_social LIKE ?";
                $bindData[] = '%'.$q.'%';

                $cpf = preg_replace('/[^0-9]/', '', $q);
                if (strlen($cpf) > 1) {
                    $cnpj = substr($cpf, 0, 2);
                    $cpf = substr($cpf, 0, 3);

                    $where .= " OR cpf LIKE ?";
                    $bindData[] = $cpf.'%';

                    $where .= " OR cnpj LIKE ?";
                    $bindData[] = $cnpj.'%';
                }
                $where .= ")";
            }

            $sql = "SELECT id, IF(cnpj='',concat(id,'- ',nome,' ',sobrenome),concat(id,'- ',razao_social)) as text FROM tblcliente";
            $sql .= " WHERE status = 1" . $where;
            $sql .= " ORDER BY nome";
            $sql .= " LIMIT ".$start.",".$resultsPerPage;

            $sqlCount = "SELECT count(id) FROM tblcliente";
            $sqlCount .= " WHERE status = 1" . $where;

            $res = Cliente::query($sql, $bindData);
            $resCount = Cliente::query($sql, $bindData);

            list($numResults) = $resCount->fetch(\PDO::FETCH_NUM);

            $moreResults = ($resultsPerPage*($page+1)) < $numResults;

            $data = [
                'results' => $res->fetchAll(\PDO::FETCH_ASSOC),
                'pagination' => ['more' => $moreResults]
            ];

            return $data;
        } catch (\Exception $e) {
            return [];
        }
    }
    // public function index () {}
    // public function show ($id) {}
    // public function create () {}
    // public function edit ($id) {}
    // public function store () {}
    // public function update ($id) {}
    // public function delete ($id)
}
