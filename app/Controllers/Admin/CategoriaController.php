<?php namespace App\Controllers\Admin;

use Core\ControllerResource;
use Core\Auth;
use Models\LeilaoCategoria;

class CategoriaController extends ControllerResource {

    public function __construct () {
        $this->setTitle('Categorias');
        $this->setBaseUrl(URL.'admin/categoria');
        $this->setModel(new LeilaoCategoria());
    }
    public function setColumnDataTable ($dataTable)
    {
        $model = $this->getModel();
        $fields = $model->getFields();
        $table = $model->getTable();

        $this->addColumnDataTable(
            $dataTable,
            'id',
            $table . '.id',
            'ID',true, true,null, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'imagem',
            $table . '.imagem',
            'Imagem',true, true,function ($v) {
                if (is_file(\PATH.'upload/'.$v)) {
                    return '<img src="'.\URL.'upload/'.$v.'" style="max-width: 50px;max-height: 50px;" />';
                 } else {
                    return '<img src="'.\URL.'theme/img/sem-foto.jpg" style="opacity:0.3;max-width: 50px;max-height: 50px;" />';
                 }
            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'pai',
            $table . '.pai',
            'Pai',true, true,function ($v, $rowData) {
                if (is_null($v) || empty($v) || $v == '-') {
                    return '-';
                }
                $res = LeilaoCategoria::find($v);
                if(!$res) {
                    return '-';
                } else {
                    return $res->nome;
                }
            }, 1
        );
        
        $this->addColumnDataTable(
            $dataTable,
            'nome',
            $table . '.nome',
            'Nome'
        );

        $this->addColumnAction($dataTable);

        return $dataTable;
    }

    public function getFieldsForm ($id)
    {
        $model = $this->getModel();

        $i = 0;

        $fieldsModel = $model->getFields();
        $fields = [];

        $fields[$i++] = $fieldsModel['id'];

        // ---------------------------
        $fields[$i] = $fieldsModel['pai'];
        $fields[$i]['label'] = 'Categoria pai';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [];
        $fields[$i]['default'] = null;

        $resCategorias = \Models\LeilaoCategoria::findAll('pai IS NULL',[], 'nome ASC');
        $fields[$i]['options'][] = ['value' => '', 'label' => 'Nenhuma'];
        while($categoria = $resCategorias->fetch()) {
            if ($id == $categoria->id) continue;
            $fields[$i]['options'][] = ['value' => $categoria->id, 'label' => $categoria->nome];
        }
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['nome'];
        $fields[$i]['label'] = 'Nome';
        $fields[$i]['classCol'] = 'col-md-4 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['imagem'];
        $fields[$i]['label'] = 'Imagem';
        $fields[$i]['classCol'] = 'col-md-6 col-xs-12';
        $fields[$i]['native_type'] = 'FILE';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        return $fields;

    }
    public function beforeSave (&$data, $id = null)
    {
        $data['pai'] = (is_null($data['pai']) || empty($data['pai'])) ? null : $data['pai'];

        if (isset($_FILES['imagem'])) {
            if (is_uploaded_file($_FILES['imagem']['tmp_name'])) {
                $name = $_FILES['imagem']['name'];
                $ext = preg_replace('/^.*([\.][^\.]*)$/', '$1', $name);

                if (!is_null($id)) {
                    $res = LeilaoCategoria::find($id);
                    if ($res != false && is_file(\PATH.'upload/'.$res->imagem)) {
                        @unlink(\PATH.'upload/'.$res->imagem);
                    }
                }
                $filename = "cat-".($id ??'-').time().$ext;
                $data['imagem'] = $filename;
                
                $extPermitidas = [
                    '.jpg','.jpeg','.gif', '.png','.svg','.webp'
                ];
                if (!in_array($ext, $extPermitidas)) {
                    throw new \Exception("Envie somente imagens do tipo: jpg, png ou gif");
                }
                
                move_uploaded_file($_FILES['imagem']['tmp_name'], \PATH.'upload/'.$filename);
            } elseif($_FILES['imagem']['error']!=\UPLOAD_ERR_OK) {
                switch ($_FILES['imagem']['error']) {
                    case \UPLOAD_ERR_INI_SIZE:
                    case \UPLOAD_ERR_FORM_SIZE:
                        throw new \Exception("Tamanho do arquivo muito grande");
                        break;
                    case \UPLOAD_ERR_CANT_WRITE:
                        throw new \Exception("Erro ao salvar imagem");
                        break;
                    case \UPLOAD_ERR_EXTENSION:
                        throw new \Exception("Envie somente imagens do tipo: jpg, png, ou gif");
                        break;
                    case \UPLOAD_ERR_NO_FILE:
                        unset($data['imagem']);
                        break;
                    default:
                        throw new \Exception("Erro ao enviar a imagem");
                        break;
                }
            }
        } else {
            unset($data['imagem']);
        }
    }

    // public function validarForm ($data, $id = null)
    // {
    //     if (is_null($id)) {
    //         if (!isset($data['senha']) || $data['senha'] == '') {
    //             throw new \Exception("Preencha a senha");
    //         }
    //     }

    //     if (!isset($data['email']) || $data['email'] == '') {
    //         throw new \Exception("Email é campo obrigatório");
    //     }

    //     return true;
    // }
    // public function index () {}
    // public function show ($id) {}
    // public function create () {}
    // public function edit ($id) {}
    // public function store () {}
    // public function update ($id) {}
    // public function delete ($id)
}
