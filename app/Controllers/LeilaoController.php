<?php namespace App\Controllers;

use Core\Controller;
use Core\View;
use Core\Auth;
use Models\Leilao;
use Models\Usuario;
use Models\Cliente;
use Models\LeilaoFoto;
use Models\Lance;

class LeilaoController extends Controller {

    public function index ()
    {
        // 0 - Não publicado
        // 1 - Publicado (Aberto)
        // 2 - Em andamento
        // 3 - Encerrado
        // 4 - Cancelado
        // 5 - Apregoado
        $data = $this->getLeiloes("status IN (1,2)");
        return View::get('leilao/index', $data);
    }

    public function arrematados () {
        $data = $this->getLeiloes("status = 3");
        $data['title'] = 'Leilões Arrematados';
        return View::get('leilao/index', $data);
    }

    public function cancelados () {
        $data = $this->getLeiloes("status = 4");
        $data['title'] = 'Leilões Cancelados';
        return View::get('leilao/index', $data);
    }

    public function apregoado () {
        $data = $this->getLeiloes("status = 5");
        $data['title'] = 'Leilão Apregoado';
        return View::get('leilao/index', $data);
    }

    public static function getLeiloes ($where, $bindData = [], $ordem = 'id desc')
    {
        $page = intval($_GET['p'] ?? 0);
        $length = 32;
        $start = $page * $length;
        $q = $_GET['q'] ?? '';

        if ($q != '') {
            $where .= " AND (tblleilao.nome LIKE ?";
            $bindData[] = '%'.$q.'%';
            
            $where .= " OR tblleilao.lote LIKE ?";
            $bindData[] = '%'.$q.'%';
            
            $where .= " OR tblleilao.processo LIKE ?)";
            $bindData[] = '%'.$q.'%';
        }

        $model = new Leilao();

        $sql = "SELECT tblleilao.*, (SELECT tf.imagem_thumb FROM tblleilao_foto tf WHERE tf.leilao_id = tblleilao.id ORDER BY ordem ASC LIMIT 1) as imagem_thumb FROM tblleilao";
        $sql .= " WHERE {$where}";
        $sql .= " ORDER BY {$ordem}";
        $sql .= " LIMIT {$start},{$length}";

        $sqlCount = "SELECT count(id) FROM tblleilao";
        $sqlCount .= " WHERE {$where}";

        $res = $model->query($sqlCount, $bindData);
        if (!$res) {
            $resultTotal = 0;
        } else {
            list($resultTotal) = $res->fetch(\PDO::FETCH_NUM);
        }

        $pages = intval($resultTotal / $length);
        if ($resultTotal % $length) {
            $pages++;
        }

        $data = [
            'title' => 'Leilões',
            // 'sql' => $sql,
            'data' => $model->querySelect($sql, $bindData),
            'countTotal' => $resultTotal,
            'q' => $q,
            'page' => $page,
            'pages' => $pages
        ];
        
        return $data;
    }

    public function show ($id, $lote = null) {
        $output = $_GET['output']??'html';
        $httpCode = 200;
        $model = new Leilao();

        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        $resUsuarioInicio = is_null($res->usuario_inicio) ? false : Usuario::find($res->usuario_inicio);
        $resUsuarioFim = is_null($res->usuario_fim) ? false : Usuario::find($res->usuario_fim);
        $resCliente = is_null($res->arrematado_por) ? false : Cliente::find($res->arrematado_por);
        $resFotos = LeilaoFoto::findAll(['leilao_id' => $res->id], [], 'ordem ASC');

        if ($output == 'json') {
            $data = [
                'id' => $id,
                'data' => $res->toArray(),
                'fotos' => $resFotos->fetchAll(\PDO::FETCH_ASSOC),
                'usuarioInicio' => !$resUsuarioInicio ? false : $resUsuarioInicio->toArray(),
                'usuarioFim' => !$resUsuarioFim ? false : $resUsuarioFim->toArray(),
                'arrematadoPor' => !$resCliente ? false : $resCliente->toArray()
            ];

            return new ResponseSimple($data, $httpCode);
        }

        $data = [
            'id' => $id,
            'data' => $res,
            'fotos' => $resFotos,
            'usuarioInicio' => $resUsuarioInicio,
            'usuarioFim' => $resUsuarioFim,
            'arrematadoPor' => $resCliente
        ];
        return View::get('leilao/show', $data);
    }
    public function setStatus ($id, $status)
    {
        try {
            $leilao = Leilao::find($id);

            if (!$leilao) {
                throw new \Exception("Leilão não encontrado.");
            }

            $leilao->status = $status;
            if ($status == 2) {
                $leilao->usuario_inicio = Auth::getIdUsuario();
            } elseif ($status == 3) {
                $leilao->usuario_fim = Auth::getIdUsuario();
                $lance = Lance::find(['leilao_id' => $id], [], 'id DESC');

                if ($lance != false) {
                    $leilao->arrematado_por = $lance->cliente_id;
                    $leilao->valor_arrematado = $lance->valor;
                }
            }

            $leilao->update($id);
            $data = [
                'status' => true,
                'message' => ''
            ];
            return $data;
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage()
            ];
            return $data;
        }
    }

    /**
     * Checa de permite fazer lance parcelado.
     * @param Int $idLeilao
     * @return Boolean
     */
    public function checarParcelado ($idLeilao)
    {
        try {
            $sql = "SELECT le.`status`, le.parcelado, IF(count(la.id)>0,0,1) FROM tblleilao le";
            $sql .= " LEFT JOIN tbllance la ON la.leilao_id = le.id AND la.parcelado = 0";
            $sql .= " WHERE le.id = ?";

            $bindData = [$idLeilao];

            $res = Lance::query($sql,$bindData);

            list($status, $leilaoParcelado, $lanceParcelado) = $res->fetch(\PDO::FETCH_NUM);

            return $status == 2 && $leilaoParcelado == 1 && $lanceParcelado == 1;
            
        } catch (\Throwable $th) {
            return false;
        }
    }

        public function iniciarContador($id)
        {
            $model = new Leilao();
            $model->update($id, ['contador' => date('Y-m-d H:i:s')]);
            return "OK";
        }

        public function pararContador ($id)
        {
            $model = new Leilao();
            $model->update($id, ['contador' => null]);
            return "OK";
        }

    public function lances ($id) {
        try {
            $bindData = [$id];
            $sql = "SELECT l.*, IF(l.cliente_id IS NULL, concat('Leiloeiro - ',u.nome),c.nome) as nome FROM tbllance l";
            $sql .= " LEFT JOIN tblusuario u ON u.id = l.usuario_id";
            $sql .= " LEFT JOIN tblcliente c ON c.id = l.cliente_id";
            $sql .= " WHERE l.leilao_id = ?";
            $sql .= " ORDER BY l.id DESC";

            $res = Lance::query($sql,$bindData);

            // if ($data->parcelado == 1) {
            // Checar se aceita parcelado
            
            $resLeilao = Leilao::query("SELECT contador FROM tblleilao WHERE id = ?", [$id]);

            list($contador) = $resLeilao->fetch(\PDO::FETCH_NUM);

            if (!is_null($contador)) {
                $contador = strtotime($contador) * 1000;
            }

            $data = [
                'status' => true,
                'message' => '',
                'parcelado' => $this->checarParcelado($id) ? 1 : 0,
                'contador' => $contador,
                'data' => $res->fetchAll(\PDO::FETCH_ASSOC)
            ];
            return $data;
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];
            return $data;
        }
    }


    public function lancesCSV ($id)
    {
        try {
            $bindData = [$id];
            $sql = "SELECT l.*, u.id as usuario_id, u.nome as usuario_nome, c.id as cliente_id, c.nome as cliente_nome, c.sobrenome,c.razao_social, c.cpf, c.cnpj FROM tbllance l";
            $sql .= " LEFT JOIN tblusuario u ON u.id = l.usuario_id";
            $sql .= " LEFT JOIN tblcliente c ON c.id = l.cliente_id";
            $sql .= " WHERE l.leilao_id = ?";
            $sql .= " ORDER BY l.id DESC";

            $res = Lance::query($sql,$bindData);

            header('Content-type:text/csv');

            $colunas = [
                ['name' => 'id', 'label' => 'id'],
                ['name' => 'data_create', 'label' => 'Data/Hora'],
                ['name' => 'valor', 'label' => 'valor'],
                ['name' => 'usuario_id', 'label' => 'Id Usuário'],
                ['name' => 'usuario_nome', 'label' => 'Nome Usuário'],
                ['name' => 'cliente_id', 'label' => 'Id Cliente'],
                ['name' => 'cliente_nome', 'label' => 'Nome Cliente'],
                ['name' => 'sobrenome', 'label' => 'Sobrenome Cliente'],
                ['name' => 'razao_social', 'label' => 'Razao Social'],
                ['name' => 'cpf', 'label' => 'CPF Cliente'],
                ['name' => 'cnpj', 'label' => 'CNPJ Cliente']
            ];

            foreach ($colunas as $col) {
                echo $col['label'] . ";";
            }
            echo "\r\n";
            while($row = $res->fetch(\PDO::FETCH_ASSOC)) {
                echo $row['id'] . ";";
                echo date('d/m/Y H:i:s', strtotime($row['data_create'])) . ";";
                echo $row['valor'] . ";";
                echo $row['usuario_id'] . ";";
                echo $row['usuario_nome'] . ";";
                echo $row['cliente_id'] . ";";
                echo $row['cliente_nome'] . ";";
                echo $row['sobrenome'] . ";";
                echo $row['razao_social'] . ";";
                echo $row['cpf'] . ";";
                echo $row['cnpj'] . ";\r\n";
            }
            exit;
        } catch (\Exception $e) {
            return new \Core\ResponseSimple('', 500);
        }
    }

    public function darLance ($id)
    {
        try {
            $leilao = Leilao::find($id);
            $model = new Lance();

            
            if ($leilao->status != 2 && $leilao->status != 5) { // 2 - Em andamento
                throw new \Exception("Leilão não esta em andamento.");
            }
            
            if (!is_null($leilao->contador)) {
                $this->pararContador($id);
            }
            $bindData = [$id];
            $sql = "SELECT valor FROM tbllance l";
            $sql .= " WHERE l.leilao_id = ?";
            $sql .= " ORDER BY l.id DESC LIMIT 1";

            $res = Lance::query($sql, $bindData);

            if (!$res || $res->rowCount() == 0) {
                $ultimoLance = 0;
            } else {
                list($ultimoLance) = $res->fetch(\PDO::FETCH_NUM);
            }

            $valor = floatval ($_POST['valor'] ?? 0);
            $parcelado = intval ($_POST['parcelado'] ?? 0);

            if (!$this->checarParcelado($id) && $parcelado == 1) {
                throw new \Exception("Este leilão não permite lances parcelados.");
            }

            if ($valor == 0) {
                $valor = $ultimoLance + $leilao->salto_lance;
            } else if($valor < $ultimoLance) {
                throw new \Exception("Valor menor que o último lance");
            }

            // if ($ultimoLance < $leilao->valor_inicial) {
            //     $valor = $leilao->valor_inicial;
            // } else {
            //     $valor = $ultimoLance + $leilao->salto_lance;
            // }

            $usuarioId = Auth::getIdUsuario();
            $clienteId = !$usuarioId? Auth::getIdCliente() : false;

            if (!$usuarioId && !$clienteId) {
                throw new \Exception("Usuário não logado.");
            }

            if (!$usuarioId) $usuarioId = null;
            if (!$clienteId) $clienteId = null;

            $model->insert([
                'usuario_id' => $usuarioId,
                'cliente_id' => $clienteId,
                'leilao_id' => $leilao->id,
                'valor' => $valor,
                'parcelado' => $parcelado
            ]);

            return $this->lances($id);
        } catch (\Exception $e) {
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'data' => []
            ];
            return $data;
        }

    }   
}
