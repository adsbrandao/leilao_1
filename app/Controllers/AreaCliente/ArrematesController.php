<?php namespace App\Controllers\AreaCliente;

use Core\{Controller, Auth, View};
use App\Controllers\LeilaoController;

class ArrematesController extends Controller {

    public function index () {
        $idCliente = Auth::getIdCliente();
        $data = LeilaoController::getLeiloes("status = 3 and arrematado_por = ?", [$idCliente]);
        $data['title'] = 'Meus Leilões Arrematados';
        return View::get('leilao/index', $data);
    }
    
}
