<?php namespace App\Controllers;

use Core\Controller;
use Core\ResponseSimple;
use Core\View;
use Core\Auth;
use Core\Query;
use Models\Cliente;
use Models\Usuario;

class ContaController extends Controller {

    private function checarLogin ()
    {
        if (Auth::isUsuario()) {
            ResponseSimple::redirect(\URL.'admin');
        } elseif (Auth::isCliente()) {
            ResponseSimple::redirect(\URL.'minha-area');
        }
    }
    public function login () {
        $this->checarLogin();
        return View::get('conta/login', [], 'login');
    }

    public function loginAction ()
    {
        try {
            $response = Auth::login(
                $_POST['email']??'',
                $_POST['senha']??''
            );
        } catch (\Exception $e) {
            $response = ['status' => 0, 'message' => 'Erro ao logar' . $e->getMessage()];
        }

        return $response;
    }

    public function logout ()
    {
        Auth::logout();
        ResponseSimple::redirect(\URL);
    }

    public function cadastro ()
    {
        $this->checarLogin();
        return View::get('conta/cadastro', [], 'login');
    }

    public function cadastroAction ()
    {
        $response = ['status' => 1, 'message' => 'Cadastrado com sucesso'];
        Query::beginTransaction();
        try {
            $tipo_pessoa = $_POST['tipo_pessoa'] ?? 0;
            $email = trim($_POST['email'] ?? '');

            if ($email == '') {
                throw new \Exception("Informe o email");
            }
            
            $where = 'email = ?';
            $bindData = [$email];

            if ($tipo_pessoa == 0) {
                $cpf = trim($_POST['cpf'] ?? null);

                if ($cpf == '') {
                    throw new \Exception("Informe o CPF");
                }
                
                $where .= ' OR cpf = ?';
                $bindData[] = $cpf;
            } elseif ($tipo_pessoa == 1) {
                $cnpj = trim($_POST['cnpj'] ?? null);

                if ($cnpj == '') {
                    throw new \Exception("Informe o CNPJ");
                }
                
                $where .= ' OR cnpj = ?';
                $bindData[] = $cnpj;
            } else {
                throw new \Exception("Tipo de pessoa desconhecido");
            }

            $cliente = Cliente::find($where, $bindData);

            if ($cliente != false) {
                throw new \Exception("Cliente já cadastrado!");
            }

            $cliente = new Cliente();

            // $cliente->imagem;
            
            $cliente->nome = $_POST['nome'] ?? null;
            $cliente->sobrenome = $_POST['sobrenome'] ?? null;
            $cliente->telefone = $_POST['telefone'] ?? null;
            $cliente->celular = $_POST['celular'] ?? null;
            $cliente->email = $_POST['email'] ?? null;
            $cliente->senha = md5($_POST['senha'] ?? null);
            $cliente->nascimento = $_POST['nascimento'] ?? null;
            if ($tipo_pessoa == 0) {
                $cliente->cpf = $_POST['cpf'] ?? '';
                $cliente->cnpj = '';
                $cliente->razao_social = '';
            } else {
                $cliente->cpf = '';
                $cliente->cnpj = $_POST['cnpj'] ?? '';
                $cliente->razao_social = $_POST['razao_social'] ?? '';
            }
            $cliente->cep = $_POST['cep'] ?? null;
            $cliente->endereco = $_POST['endereco'] ?? null;
            $cliente->numero = $_POST['numero'] ?? null;
            $cliente->complemento = $_POST['complemento'] ?? null;
            $cliente->bairro = $_POST['bairro'] ?? null;
            $cliente->estado = $_POST['estado'] ?? null;
            $cliente->cidade = $_POST['cidade'] ?? null;

            $cliente->status = 2; // Novo
            $cliente->data_create = date('Y-m-d H-i-s');
            $cliente->data_update =  date('Y-m-d H-i-s');
            $cliente->data_acesso = date('Y-m-d H-i-s');

            // Anexar documentos
            


            $idCliente = $cliente->insert();

            $cliente->documento1 = $this->upload('documento1', $idCliente);
            $cliente->documento2 = $this->upload('documento2', $idCliente);
            $cliente->documento3 = $this->upload('documento3', $idCliente);

            $cliente->update($idCliente);
       
            Query::commit();

        } catch (\Exception $e) {
            $response = ['status' => 0, 'message' => $e->getMessage(), 'trace' => $e->getTrace()];
            Query::rollBack();
        }

        return $response;
    }

    public function upload ($campo, $idCliente) {
        if (isset($_FILES[$campo]['tmp_name'])) {
            $tmpName = $_FILES[$campo]['tmp_name'];
            if (is_uploaded_file($tmpName)) {
                $name = $_FILES[$campo]['name'];
                $ext = preg_replace('/^.*[\.]([^\.]*)$/', '$1', $name);
                $pathArquivo = 'upload/documentos/'.$campo.$idCliente.time();
                $pathArquivo = $pathArquivo.'.'.$ext;

                $extPermitidas = [
                    'jpg',
                    'jpeg',
                    'pdf',
                    'zip',
                    'rar',
                    'gif',
                    'png',
                    'svg',
                    'webp'
                ];

                if (!in_array($ext, $extPermitidas)) {
                    throw new \Exception("Envie somente imagens do tipo: pdf, zip, jpg, png ou gif");
                }
                
                move_uploaded_file($tmpName, \PATH.$pathArquivo);
                return $pathArquivo;
            }
        }

        return false;
    }

    public function validar ()
    {
        $this->checarLogin();
    }

    public function esqueciSenha ()
    {
        $this->checarLogin();
        return View::get('conta/esquecisenha', [], 'login');
    }

    public function esqueciSenhaAction ()
    {
        try {
            $email = $_POST['email']?? '';
            $usuario = Usuario::find(['email' => $email]);
            $cliente = Cliente::find(['email' => $email]);

            if (!$usuario && !$cliente) {
                throw new \Exception("Email não cadastrado");
            }

            // TODO Gerar email para enviar

            return ['status' => true, 'message' => ''];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function testeCadastro ()
    {
        try {
            $id = intval($_POST['id']?? 0);
            $email = $_POST['email']?? '';
            $cpf = $_POST['cpf']?? '';
            $cnpj = $_POST['cnpj']?? '';

            $where = "id <> {$id} AND (1=0";
            $bindData = [];

            if (!empty($email)) {
                $where .= " OR email = ?";
                $bindData[] = $email;
            }

            if (!empty($cpf)) {
                $cpf = preg_replace('/[^0-9]/', '', $cpf);
                if (strlen($cpf) == 11) {
                    $where .= " OR cpf = ?";
                    $bindData[] = $cpf;

                    $where .= " OR cpf = ?";
                    $bindData[] = preg_replace('/^([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})$/', '$1.$2.$3-$4', $cpf);
                }
            }

            if (!empty($cnpj)) {
                $cnpj = preg_replace('/[^0-9]/', '', $cnpj);
                if (strlen($cnpj) == 14) {
                    $where .= " OR cnpj = ?";
                    $bindData[] = $cnpj;

                    $where .= " OR cnpj = ?";
                    $bindData[] = preg_replace('/^([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})$/', '$1.$2.$3/$4-$5', $cnpj);
                }
            }

            $where .= ")";

            $count = Cliente::sizeof($where, $bindData);
            if (!$count || $count == 0) {
                return ['status' => true, 'message' => 'Cadastro novo', 'w' => $where,'b' => $bindData];
            } else {
                $cliente = Cliente::find($where, $bindData);
                $email = explode('@', $cliente->email);
                $email[0] = substr($email[0], 0, 3).str_repeat('*', strlen(substr($email[0], 3, -2))).substr($email[0], -2);
                $email = implode('@', $email);

                return ['status' => false, 'message' => 'Cliente já cadastrado com o email '.$email];
            }
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

}
