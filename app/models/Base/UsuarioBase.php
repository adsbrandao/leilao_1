<?php namespace Models\Base;

use Core\Model;

class UsuarioBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $grupo_id;
    * @var mixed $status;
    * @var mixed $imagem;
    * @var mixed $nome;
    * @var mixed $telefone;
    * @var mixed $celular;
    * @var mixed $email;
    * @var mixed $senha;
    * @var mixed $data_create;
    * @var mixed $data_update;
    * @var mixed $data_acesso;
    */

public function __construct ($data = [])
{
    $this->setTable('tblusuario');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
