<?php namespace Models\Base;

use Core\Model;

class LanceBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $data_create;
    * @var mixed $usuario_id;
    * @var mixed $cliente_id;
    * @var mixed $leilao_id;
    * @var mixed $valor;
    */

public function __construct ($data = [])
{
    $this->setTable('tbllance');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
