<?php namespace Models\Base;

use Core\Model;

class LeilaoBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $status;
    * @var mixed $data_create;
    * @var mixed $data_update;
    * @var mixed $data_inicio;
    * @var mixed $data_fim;
    * @var mixed $lote;
    * @var mixed $usuario_inicio;
    * @var mixed $usuario_fim;
    * @var mixed $arrematado_por;
    * @var mixed $categoria_id;
    * @var mixed $nome;
    * @var mixed $processo;
    * @var mixed $descricao;
    * @var mixed $salto_lance;
    * @var mixed $valor_inicial;
    * @var mixed $valor_arrematado;
    */

public function __construct ($data = [])
{
    $this->setTable('tblleilao');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
