<?php namespace Models\Base;

use Core\Model;

class PermissaoBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $nome;
    * @var mixed $request;
    * @var mixed $method;
    * @var mixed $permite_cliente;
    */

public function __construct ($data = [])
{
    $this->setTable('tblpermissao');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
