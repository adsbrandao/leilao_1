<?php namespace Models\Base;

use Core\Model;

class ClienteBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $status;
    * @var mixed $imagem;
    * @var mixed $nome;
    * @var mixed $sobrenome;
    * @var mixed $telefone;
    * @var mixed $celular;
    * @var mixed $email;
    * @var mixed $senha;
    * @var mixed $nascimento;
    * @var mixed $cpf;
    * @var mixed $cnpj;
    * @var mixed $razao_social;
    * @var mixed $cep;
    * @var mixed $endereco;
    * @var mixed $numero;
    * @var mixed $complemento;
    * @var mixed $bairro;
    * @var mixed $estado;
    * @var mixed $cidade;
    * @var mixed $data_create;
    * @var mixed $data_update;
    * @var mixed $data_acesso;
    * @var mixed $documento_frete;
    * @var mixed $documento_verso;
    */

public function __construct ($data = [])
{
    $this->setTable('tblcliente');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
