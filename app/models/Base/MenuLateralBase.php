<?php namespace Models\Base;

use Core\Model;

class MenuLateralBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $titulo;
    * @var mixed $caminho;
    * @var mixed $icone;
    * @var mixed $pai;
    * @var mixed $permissao_id;
    * @var mixed $ordem;
    * @var mixed $permite_cliente;
    */

public function __construct ($data = [])
{
    $this->setTable('tblmenu_lateral');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
