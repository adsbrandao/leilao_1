<?php namespace Models\Base;

use Core\Model;

class LeilaoFotoBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $leilao_id;
    * @var mixed $ordem;
    * @var mixed $imagem_original;
    * @var mixed $imagem_thumb;
    */

public function __construct ($data = [])
{
    $this->setTable('tblleilao_foto');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
