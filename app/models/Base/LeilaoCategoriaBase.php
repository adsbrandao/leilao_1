<?php namespace Models\Base;

use Core\Model;

class LeilaoCategoriaBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $pai;
    * @var mixed $nome;
    * @var mixed $imagem;
    */

public function __construct ($data = [])
{
    $this->setTable('tblleilao_categoria');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
