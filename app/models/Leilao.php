<?php namespace Models;

use Models\Base\LeilaoBase;

class Leilao extends LeilaoBase {
    const NAO_PUBLICADO = 0;
    const PUBLICADO = 1;
    const ANDAMENTO = 2;
    const ENCERRADO = 3;
    const CANCELADO = 4;
}
