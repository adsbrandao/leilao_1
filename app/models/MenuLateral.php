<?php namespace Models;

use Core\Auth;
use Models\Base\MenuLateralBase;

class MenuLateral extends MenuLateralBase {
    static $_menus;
    public static function getData ($pai = null)
    {
        if (!is_null(MenuLateral::$_menus) && is_null($pai)) {
            return MenuLateral::$_menus;
        }
        $dataUser = Auth::get();
        if (!$dataUser) return [];

        $permissoes = Usuario::getPermissoes();
        $permissoes = "'".implode("','", $permissoes)."'";

        $where = "1=1";
        $bindData = [];

        if (is_null($pai)) {
            $where = "pai IS NULL";
        } else {
            $bindData[] = $pai;
            $where = "pai = ?";
        }

        $where .= " AND (";

        $where .= "permissao_id IS NULL";
        $where .= " OR permissao_id IN ({$permissoes})";

        if ($dataUser->cliente_status == 1) {
            $where .= " OR tblmenu_lateral.permite_cliente > 0";
            $where .= " OR p.permite_cliente > 0";
        } elseif ($dataUser->cliente_status == 2) {
            $where .= " OR tblmenu_lateral.permite_cliente = 2";
            $where .= " OR p.permite_cliente = 2";
        }
        $where .= ")";

        $sql = "SELECT tblmenu_lateral.*, p.nome, p.request, p.method FROM tblmenu_lateral
            LEFT JOIN tblpermissao p ON tblmenu_lateral.permissao_id = p.id
            WHERE ".$where . " ORDER BY ordem";

        $res = MenuLateral::query($sql, $bindData);

        $menus = [];
        while ($row = $res->fetch(\PDO::FETCH_ASSOC)) {
            $row['filhos'] = MenuLateral::getData($row['id']);
            // $row['filhos'] = [];
            $menus[] = $row;
        }

        MenuLateral::$_menus = $menus;

        return $menus;
    }
}
