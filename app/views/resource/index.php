<?php

use Core\View;

View::setTitle($title ?? null);

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb($title ?? null, $baseURL);

View::appendTitleAction('<a href="'.$baseURL.'/create" class="btn btn-primary"><i class="fa fa-plus"></i> Cadastrar</a>');
View::useComponent('modals');
?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Leilões abertos</h5>
    </div>
    <div class="ibox-content">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-resource">
            <thead>
                <tr>
                <?php foreach($columns as $col) { ?>
                <th<?=is_null($col['width'])?'':' width="'.$col['width'].'"';?>><?=$col['label'];?></th>
                <?php } ?>
                </tr>
            </thead>
            <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<?php
viewModalFadeIn(
  'dvModalDelete',
  'Deseja deletar este registro?',
  '<p>Ao deletar este registro não poderá ser recuperado</p>',
  '<button type="button" class="btn btn-white" data-dismiss="modal">Não</button> '.
  '<button onclick="deleteRecordConfirmacao()" type="button" class="btn btn-danger">Sim, deletar!</button>'
);

viewModalFadeIn(
  'dvModalFalha',
  '&nbsp;',
  '&nbsp;',
  '<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button> '
);?>

<script src="<?=URL?>theme/js/plugins/dataTables/datatables.min.js"></script>
<script>
var insDataTable;
var idDelete = 0;

function deleteRecord (id) {
  idDelete = id;
  $('#dvModalDelete').modal('show');
}

function deleteRecordConfirmacao () {
  $('#dvModalDelete .modal-body').html('<p><i class="fa fa-refresh fa-spin"></i> Aguarde</p>');
  http('<?=$baseURL?>/' + idDelete, 'DELETE')
  .then((res) => {
    //insDataTable.draw();
    if (res.ok) {
      insDataTable.ajax.reload();
    } else {
      let msgError = '';
      
      res.text().then(r => {
        $('#dvModalFalha .modal-body').html(r);
      });

      if (res.status == 403) {
        msgError = 'Não permitido';
      } else if (res.status == 404) {
        msgError = 'Não encontrado';
      }
      $('#dvModalFalha .modal-title').html('Falha ao deletar!');
      $('#dvModalFalha .modal-title').addClass('text-danger');
      $('#dvModalFalha .modal-body').html(msgError);
      $('#dvModalFalha').modal('show');
    }
    $('#dvModalDelete').modal('hide');
    $('#dvModalDelete .modal-body').html('<p>Ao deletar este registro não poderá ser recuperado</p>');
  }).catch((reason) => {
      console.log('Error', reason);
      $('#dvModalDelete').modal('hide');
      $('#dvModalDelete .modal-body').html('<p>Ao deletar este registro não poderá ser recuperado</p>');

      $('#dvModalFalha .modal-title').html('Falha ao deletar!');
      $('#dvModalFalha .modal-title').addClass('text-danger');
      $('#dvModalFalha .modal-body').html(reason);
      $('#dvModalFalha').modal('show');
    });
}

$(document).ready(function(){
  insDataTable = $('.dataTables-resource').DataTable({
    serverSide: true,
    processing: true,
    ajax: {
      url: '<?=$baseURL?>?output=json',
      type: 'GET'
    },
    columns: [
      // { data: 'row_action' },
      <?php
      $sep = '';
      foreach($columns as $col) { ?>
        <?=$sep;?>{
            data: '<?=$col['name']?>',
            sortable: <?=$col['sortable']?'true':'false';?>,
            orderable: <?=$col['searchable']?'true':'false';?>
        }
        <?php
        $sep = ',';
        }?>
    ],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: []
  });
});
</script>