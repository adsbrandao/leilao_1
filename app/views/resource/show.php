<?php

use Core\View;
use Core\StringCase;

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb($title??'', $baseURL);

View::setTitle('Ver | ' . ($title ?? ''));
View::addBreadcrumb('Ver', $baseURL);

// View::useComponent('modals');
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
<?php foreach ($fields as $field) {
    $name = $field['name'];
    $label = $field['label']?? StringCase::WordCapCase($name);
    $type = strtoupper($field['native_type']?? 'VAR_STRING');
    $default = $field['default']?? null;
    $len = $field['maxlen'] ?? intval(($field['len']?? 0) / 3);
    $precision = $field['precision']?? 0;
    $classCol = $field['classCol']?? 'col-md-3 col-xs-12';
    $options =  $field['options']?? [];
    $onchange =  $field['onchange']?? '';

    $isPrimaryKey = $primaryKey == $name;
    $value = $data[$name]??$default;
    $value = is_null($value) ? '' : $value;

    if ($isPrimaryKey) {
        $type = "HIDDEN";
    }

    if ($type == "HIDDEN") {
    } elseif($type == 'SEPARADOR') {
        echo '<div class="clearfix"></div>';
        echo '<h2>'.$label.'</h2> ';
    } else {
        echo '<div class="'.$classCol.'" style="padding-bottom: 20px;">';
        echo '<label for="f_'.$name.'">'.$label.'</label> ';
        // echo $value;
        switch ($type) {
            case 'TIMESTAMP':
            case 'DATETIME':
                $value = strtotime($value);
                if (is_null($value) || $value < 100) {
                    $value = '-';
                } else {
                    $value = date("d-m-Y H:i:s", $value);
                }
                echo '<p>'.$value.'</p> ';
                break;
            case 'DATE':
                $value = strtotime($value);
                if (is_null($value) || $value < 100) {
                    $value = '-';
                } else {
                    $value = date("d-m-Y", $value);
                }
                echo '<p>'.$value.'</p> ';
                break;
            case 'LONG':
            case 'LONGLONG':
            case 'TINY':
            case 'DOUBLE':
            case 'DECIMAL':
                echo '<p>'.$value.'</p>';
                break;
            case 'PASSWORD':
                echo '<p>****</p>';
                break;
            case 'SELECT':
            case 'RADIO':
                foreach ($options as $option) {
                    if ($value != $option['value']) continue;
                    echo '<p>'.$option['label'].'</p>';
                }
                break;
            case 'TEXTAREA':
            case 'TEXT':
                echo '<div>'.$value.'</div> ';
                break;
            case 'FILE':
                if (is_file(\PATH.'upload/'.$value)) {
                    echo '<img src="'.\URL.'upload/'.$value.'" style="margin:20px 0;max-width:400px;max-height:400px" />';
                }
                break;
            default:
                $maxLength = $len>0? ' maxlength="'.$len.'"' : '';
                echo '<p>'.$value.'</p> ';
                break;
        }
        echo '</div>';
    }

}
?>
    <div class="clearfix"></div>
    <div class="hr-line-dashed"></div>
    <div style="display:flex;flex-wrap:wrap;">
        <div>
            <a href="<?=$baseURL.'/'.$id."/edit";?>" id="btn_editar" class="btn btn-default"><i class="fa fa-pencil"></i> Editar</a>
            <a href="<?=$baseURL;?>" id="btn_voltar" class="btn btn-default">Voltar</a>
        </div>
    </div>
    </div>
</div>
<?=$appendHTML??'';?>
