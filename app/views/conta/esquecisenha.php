<?php

use Core\View;

View::setTitle('Cadastro');
?>

<div class="container">
    <div>
        <div class="pull-left" style="padding: 10px 0;"><img
            style="max-height: 70px;"
            src="<?=URL?>theme/img/logo.svg"
            alt="Norte de Minas Leilões">
        </div>
        <div class="pull-left" style="padding: 10px;">
            <h1 style="line-height: 70px;margin: 0;padding: 0;">Recuperar login</h1>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div id="dv_frm">
            <form id="frm_esqueci" role="form">
                <input type="hidden" name="tipo_pessoa" id="tipo_pessoa" value="0">

                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-4 form-group">
                        <p>Informe o seu <strong>e-mail</strong> cadastrado para recuperação do login.</p>
                        <label for="email">Email</label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            required
                            class="form-control">
                        <div id="dv_msg"></div>
                    </div>
                </div>
                
                <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-md-offset-4">
                            <button
                                id="btn_proximo"
                                class="btn btn-primary m-t-n-xs"
                                type="submit">Próximo</button>
                            <a
                                href="<?=URL;?>login"
                                class="btn btn-default m-t-n-xs">Voltar ao login</a>
                        </div>
                    </div>
                </div>
            </form>
            </div>
            <div id="dv_mensagem" style="display:none;">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-4">
                        <h1 class="text-primary">Email enviado!</h1>
                        <p>Confira em seu email o link para recuperação de login.</p>
                        <a href="<?=URL;?>login" class="btn btn-primary btn-lg">Voltar ao login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const showError = (msg, icon = 'fa fa-times', type = 'danger') => {
        showMsg('#dv_msg',msg, icon, type);
        setTimeout(() => {
            showMsg('#dv_msg','');
        }, 2000);
    }

    $('#frm_esqueci').submit(function (e) {
        e.preventDefault();
        
        $('#btn_proximo')
            .addClass('disabled')
            .prop('disabled', true)
            .html('<i class="fa fa-refresh fa-spin"></i> Próximo');

        sendForm('<?=URL;?>esqueci-senha', this)
            .then(res => {
                if (res.status) {
                    showMsg('#dv_msg','');
                    // window.location = res.redirect;
                    $('#dv_frm').hide();
                    $('#dv_mensagem').show();
                } else {
                    showError(res.message, 'fa fa-times', 'danger');
                }
            })
            .catch(reason => {
                console.log(reason)
                showError('Erro ao tentar recuperar', 'fa fa-times', 'danger');
            })
            .finally(() => {
                $('#btn_proximo')
                    .removeClass('disabled')
                    .prop('disabled', false)
                    .html('Próximo');
            });

    });
</script>