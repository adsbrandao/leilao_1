<?php

use Core\View;

View::setTitle('Login');

?>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img
                style="max-width: 80%;"
                src="<?=URL?>theme/img/logo.svg"
                alt="Norte de Minas Leilões">
        </div>
        <h2>Login</h2>
        <div>
            <div id="dv_login">
                <form id="frm_login" role="form">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="senha" class="form-control">
                    </div>
                    <div>
                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Entrar</strong></button>
                    </div>
                    <div>
                        <a href="<?=URL?>esqueci-senha" class="btn btn-sm btn-link pull-right m-t-n-xs">Esqueci minha senha</a>
                    </div>
                </form>
            </div>
            <div id="dv_msg_login" onclick="showMsgLogin('');"></div>
        </div>
        <div class="clearfix"></div>
        <div class="text-center" style="margin: 50px 0;">
            <h4>Não tem conta ainda?</h4>
            <a href="<?=URL?>cadastro" class="btn btn-primary btn-lg">Crie sua conta agora</a>
        </div>
        <div style="text-align:center;">
            <a href="<?=URL?>" class="btn btn-link btn-lg"><i class="fa fa-long-arrow-left"></i> Voltar para o site</a>
        </div>
    </div>
</div>

<script>
    function showMsgLogin (msg, icon = '', type = 'primary') {
        if (msg == '') {
            $('#dv_msg_login').removeClass('active');
            $('#dv_login').removeClass('hidden');
        } else {
            let html = '<span class="fa-2x">';
            if (icon != '') {
                html += `<i class="${icon}"></i> `;
            }
            html += msg + '</span>';
            $('#dv_msg_login').html(html);
            $('#dv_msg_login').removeClass('primary danger').addClass('active ' + type);
            $('#dv_login').addClass('hidden');
        }
    }

    $('#frm_login').submit(function (e) {
        e.preventDefault();
        
        showMsgLogin('Aguarde', 'fa fa-refresh fa-spin fa-2x');

        sendForm('<?=URL;?>login', this)
            .then(res => {
                if (res.status) {
                    showMsgLogin('');
                    window.location = res.redirect;
                } else {
                    showMsgLogin(res.message, 'fa fa-times', 'danger');
                    setTimeout(() => {
                        showMsgLogin('');
                    }, 2000);
                }
            })
            .catch(reason => {
                showMsgLogin('Erro ao logar', 'fa fa-times', 'danger');
                setTimeout(() => {
                    showMsgLogin('');
                }, 2000);
            });

    });
</script>