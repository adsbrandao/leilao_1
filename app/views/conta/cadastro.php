<?php

use Core\View;

View::setTitle('Cadastro');
?>

<div class="container">
    <div>
        <div class="pull-left" style="padding: 10px 0;"><img
            style="max-height: 70px;"
            src="<?=URL?>theme/img/logo.svg"
            alt="Norte de Minas Leilões">
        </div>
        <div class="pull-left" style="padding: 10px;">
            <h1 style="line-height: 70px;margin: 0;padding: 0;">Cadastro</h1>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div id="dv_frm">
            <form id="frm_cadastro" role="form"  enctype="multipart/form-data">
                <input type="hidden" name="tipo_pessoa" id="tipo_pessoa" value="0">
                <h3>Dados pessoais</h3>
                <div class="row">                
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="nome">Nome</label>
                        <input
                            type="text"
                            name="nome"
                            id="nome"
                            required
                            pattern="[a-zA-Z0-9 ]{2}[a-zA-Z0-9 ]+"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="sobrenome">Sobrenome</label>
                        <input
                            type="text"
                            name="sobrenome"
                            id="sobrenome"
                            required
                            pattern="[a-zA-Z0-9 ][a-zA-Z0-9 ]+"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="nascimento">Data de nascimento</label>
                        <input
                            type="date"
                            name="nascimento"
                            id="nascimento"
                            required
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="tipo">Tipo pessoa</label><br />
                        <div class="btn-group">
                            <button id="btn_fisica" class="btn btn-primary">Física</button>
                            <button id="btn_juridica" class="btn btn-default">Jurídica</button>
                        </div>
                    </div>
                    <div
                        class="col-xs-12 col-md-3 form-group show-fisica">
                        <label for="cpf">CPF</label>
                        <input
                            type="text"
                            name="cpf"
                            id="cpf"
                            pattern="[0-9]{3}[.][0-9]{3}[.][0-9]{3}[-][0-9]{2}"
                            placeholder="000.000.000-00"
                            class="form-control cpf">
                    </div>
                    <div
                        class="col-xs-12 col-md-3 form-group show-juridica"
                        style="display: none;">
                        <label for="razao_social">Razão Social</label>
                        <input
                            type="text"
                            name="razao_social"
                            id="razao_social"
                            pattern=".+" class="form-control">
                    </div>
                    <div
                        class="col-xs-12 col-md-3 form-group show-juridica"
                        style="display: none;">
                        <label for="cnpj">CNPJ</label>
                        <input
                            type="text"
                            name="cnpj"
                            id="cnpj"
                            pattern="[0-9]{2}[.][0-9]{3}[.][0-9]{3}[/][0-9]{4}[-][0-9]{2}"
                            placeholder="00.000.000/0000-00"
                            class="form-control cnpj">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="telefone">Telefone</label>
                        <input
                            type="text"
                            name="telefone"
                            id="telefone"
                            placeholder="(00)0000-0000"
                            class="form-control telefone">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="celular">Celular</label>
                        <input
                            type="text"
                            name="celular"
                            id="celular"
                            placeholder="(00)00000-0000"
                            required
                            pattern="[\(][0-9]{2}[\)][0-9]{5}[-][0-9]{4}"
                            class="form-control celular">
                    </div>
                </div>
                <h3>Informação para login</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="email">E-mail</label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            required
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="senha">Senha</label>
                        <input
                            type="password"
                            name="senha"
                            id="senha"
                            required
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="resenha">Confirme a senha</label>
                        <input
                            type="password"
                            name="resenha"
                            id="resenha"
                            required
                            class="form-control">
                    </div>
                </div>
                <h3>Endereço</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="cep">CEP</label>
                        <input
                            type="text"
                            name="cep"
                            id="cep"
                            onchange="onChangeCep(this.value)"
                            required
                            pattern="[0-9]{5}[-][0-9]{3}"
                            class="form-control cep">
                    </div>
                    <div class="col-xs-12 col-md-6 form-group">
                        <label for="endereco">Endereço</label>
                        <input
                            type="text"
                            name="endereco"
                            id="endereco"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="numero">Número</label>
                        <input
                            type="text"
                            name="numero"
                            id="numero"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="complemento">Complemento</label>
                        <input
                            type="text"
                            name="complemento"
                            id="complemento"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="bairro">Bairro</label>
                        <input
                            type="text"
                            name="bairro"
                            id="bairro"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="cidade">Cidade</label>
                        <input
                            type="text"
                            name="cidade"
                            id="cidade"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="estado">Estado</label>
                        <input
                            type="text"
                            name="estado"
                            id="estado"
                            class="form-control">
                    </div>
                </div>

                <!-- ANEXAR DOCUMENTOS -->
                <h3>Anexar documentos</h3>
                <div class="row">
                <div class="col-xs-12 col-md-3 form-group" style="margin-top:10px;">
                <label>Documento frente:</label>
                <input type="file" name="documento1" required class="form-control documento" />
                </div>

                <div class="col-xs-12 col-md-3 form-group" style="margin-top:10px;">
                <label>Documento verso:</label>
                <input type="file" name="documento2" required class="form-control documento"/>
                </div>

                <div class="col-xs-12 col-md-3 form-group" style="margin-top:10px;">
                <label>Anexo complementar (se necessário):</label>
                <input type="file" name="documento3" class="form-control documento"/>
                </div>
                
                <div id="dv_msg"></div>
                <div class= "col-xs-12 col-md-3 form-group"
                style="margin-top: 30px;">
                    <button
                        id="btn_cadastrar"
                        class=" btn btn-primary m-t-n-xs"
                        type="submit">Cadastrar</button>
                    <a  href="<?=URL;?>login"
                        class="btn btn-default m-t-n-xs"
                        type="submit">Voltar ao login</a>
                </div>
            </form>
            </div>
            <div id="dv_mensagem" style="display:none;">
                <h1 class="text-primary">Você foi cadastrado com sucesso!</h1>
                <p>Mas ainda não está ativo em nossa plataforma,
                nossa equipe primeiro irá validar seus dados.</p>
                <a href="<?=URL;?>" class="btn btn-primary btn-lg">Voltar ao site</a>
            </div>
        </div>
    </div>
</div>

<script>
    const testarCadastro = function () {
        let email = $('#email').val();
        let cpf = $('#cpf').val();
        let cnpj = $('#cnpj').val();
        let tipoPessoa = $('#tipo_pessoa').val();
        if (tipoPessoa == 0) {
            cnpj = '';
        } else {
            cpf = '';
        }

        testeCadastro(
            0,
            email,
            cpf,
            cnpj,
            (msg) => {                
                showError('');
                $('#btn_cadastrar')
                    .removeClass('disabled')
                    .prop('disabled', false)
                    .html('Cadastrar');
            },
            (msg) => {
                showError(msg);
                $('#btn_cadastrar')
                    .addClass('disabled')
                    .prop('disabled', true)
                    .html('Cadastrar');
            });
    };

    $('#email').change(testarCadastro);
    $('#cpf').change(function () {
        if(testCPF($(this).val())) {
            testarCadastro();
        } else {
            showError('CPF Inválido');
        }
    });
    $('#cnpj').change(function () {
        if(testCNPJ($(this).val())) {
            testarCadastro();
        } else {
            showError('CNPJ Inválido');
        }
    });
    $('#btn_fisica').click(function (e) {
        e.preventDefault();
        $('.show-fisica').show();
        $('.show-juridica').hide();
        $('#tipo_pessoa').val(0);
        $('#btn_fisica').addClass('btn-primary').removeClass('btn-default');
        $('#btn_juridica').addClass('btn-default').removeClass('btn-primary');
        $('.show-fisica input').prop('required', true);
        $('.show-juridica input').prop('required', false);
    });

    $('#btn_juridica').click(function (e) {
        e.preventDefault();
        $('.show-juridica').show();
        $('.show-fisica').hide();
        $('#tipo_pessoa').val(1);
        $('#btn_juridica').addClass('btn-primary').removeClass('btn-default');
        $('#btn_fisica').addClass('btn-default').removeClass('btn-primary');
        $('.show-juridica input').prop('required', true);
        $('.show-fisica input').prop('required', false);
    });

    const showError = (msg, icon = 'fa fa-times', type = 'danger') => {
        showMsg('#dv_msg',msg, icon, type);
        setTimeout(() => {
            showMsg('#dv_msg','');
        }, 2000);
    }

    $('#frm_cadastro').submit(function (e) {
        e.preventDefault();

        let senha = $('#senha').val();
        let resenha = $('#resenha').val();
        let tipoPessoa = $('#tipo_pessoa').val();
        let cpf = $('#cpf').val();
        let cnpj = $('#cnpj').val();

        if (senha != resenha) {
            showError('As duas senha devem ser iguais', 'fa fa-times', 'danger');
            return false;
        }

        if (tipoPessoa == 0) {
            if(!testCPF(cpf)) {
                showError('CPF Inválido');
                return false;
            }
        } else {
            if (!testCNPJ(cnpj)) {
                showError('CNPJ Inválido');
                return false;
            }
        }
        
        $('#btn_cadastrar')
            .addClass('disabled')
            .prop('disabled', true)
            .html('<i class="fa fa-refresh fa-spin"></i> Cadastrar');

        sendForm('<?=URL;?>cadastro', this)
            .then(res => {
                if (res.status) {
                    showMsg('#dv_msg','');
                    // window.location = res.redirect;
                    $('#dv_frm').hide();
                    $('#dv_mensagem').show();
                } else {
                    showError(res.message, 'fa fa-times', 'danger');
                }
            })
            .catch(reason => {
                console.log(reason)
                showError('Erro ao cadastrar', 'fa fa-times', 'danger');
            })
            .finally(() => {
                $('#btn_cadastrar')
                    .removeClass('disabled')
                    .prop('disabled', false)
                    .html('Cadastrar');
            });

    });
</script>