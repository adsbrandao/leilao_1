<?php

use Core\View;
use Core\StringCase;

$isCadastro = $id == 0;
$isEditar = !$isCadastro;

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb($title, $baseURL);

if ($isCadastro) {
    $id = 0;
    View::setTitle('Cadastro | ' . ($title ?? ''));
    View::addBreadcrumb('Cadastro', $baseURL);
} else {
    $id = intval($id);
    View::setTitle('Editar | ' . ($title ?? ''));
    View::addBreadcrumb('Editar', $baseURL);
}

$urlSave = $baseURL;

if ($isEditar) {
    $urlSave .= '/'.$id;
}
// View::useComponent('modals');
?>

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Leilão</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"> Lances</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <form id="frm_edit" action="#" method="post" enctype="multipart/form-data">
<?php foreach ($fields as $field) {
    $name = $field['name'];
    $label = $field['label']?? StringCase::WordCapCase($name);
    $type = strtoupper($field['native_type']?? 'VAR_STRING');
    $default = $field['default']?? null;
    $len = $field['maxlen'] ?? intval(($field['len']?? 0) / 3);
    $precision = $field['precision']?? 0;
    $classCol = $field['classCol']?? 'col-md-3 col-xs-12';
    $options =  $field['options']?? [];
    $onchange =  $field['onchange']?? '';

    $isPrimaryKey = $primaryKey == $name;
    $value = $data[$name]??$default;
    $value = is_null($value) ? '' : $value;

    if ($onchange != '') {
        $onchange = ' onchange="'.$onchange.'"';
    }

    if ($isPrimaryKey) {
        $type = "HIDDEN";
        if ($isCadastro) {
            $default = null;
        }
    }

    if ($type == "HIDDEN") {
        echo '<input type="hidden" id="f_'.$name.'" name="'.$name.'" value="'.$value.'" /> ';
    } elseif($type == 'SEPARADOR') {
        echo '<div class="clearfix"></div>';
        echo '<h2>'.$label.'</h2> ';
    } else {
        echo '<div class="'.$classCol.'" style="padding-bottom: 20px;">';
        echo '<label for="f_'.$name.'">'.$label.'</label> ';
        // echo $value;
        switch ($type) {
            case 'TIMESTAMP':
            case 'DATETIME':
                $value = date("Y-m-d\TH:i:s", strtotime($value));
                echo '<input '.$onchange.' type="datetime-local" id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
            case 'DATE':
                echo '<input '.$onchange.' type="date" id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
            case 'TIME':
                echo '<input '.$onchange.' type="text" data-mask="00:00" id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
            case 'LONG':
            case 'LONGLONG':
            case 'TINY':
            case 'DOUBLE':
            case 'DECIMAL':
                $step = ($precision>=0)?'' : ' step="'.(1 / (10 ^ $precision)) . '"';
                echo '<input '.$onchange.' type="number"'.$step.' id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
            case 'TIME':
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" value="" class="form-control" /> ';
                break;
            case 'PASSWORD':
                echo '<input '.$onchange.' type="password" id="f_'.$name.'" name="'.$name.'" value="" class="form-control" /> ';
                break;
            case 'SELECT':
                echo '<select '.$onchange.' id="f_'.$name.'" name="'.$name.'" class="form-control">';
                foreach ($options as $option) {
                    $selected = $value == $option['value'] ? ' selected' : '';
                    echo '<option value="'.$option['value'].'"'.$selected.'>'.$option['label'].'</option> ';
                }
                echo '</select>';
                break;
            case 'SELECT_AJAX':
                echo '<select '.$onchange.' id="f_'.$name.'" name="'.$name.'" class="form-control select2">';
                if (!is_null($options['default'])) {
                    echo '<option value="'.$options['default']['value'].'" selected>'.$options['default']['label'].'</option> ';
                }
                echo '</select>';

                $jsOnload .= "$(`#f_{$name}`).select2({
                    ajax: {
                    url: '{$options['url']}',
                    dataType: 'json',
                    data: function (params) {
                        return {
                        q: params.term,
                        page: params.page
                        }
                    }
                    }
                });";
                break;
            case 'RADIO':
                $i = 0;
                foreach ($options as $option) {
                    $checked = $value == $option['value'] ? ' checked' : '';
                    echo '<label><input '.$onchange.' type="radio" '.$checked.' id="f_'.$name.'_'.$i.'" name="'.$name.'" value="" class="form-control" /> '.$option['label'].'</label><br /> ';
                    $i++;
                }
                break;
            case 'TEXTAREA':
            case 'TEXT':
                $maxLength = $len>0? ' maxlength="'.$len.'"' : '';
                echo '<textarea '.$onchange.' style="min-height:50px;" '.$maxLength.' id="f_'.$name.'" name="'.$name.'" class="form-control js-auto-size">'.$value.'</textarea> ';
                break;
            case 'FILE':
                echo '<input '.$onchange.' type="file" id="f_'.$name.'" name="'.$name.'" class="form-control" /> ';
                if (is_file(\PATH.'upload/'.$value)) {
                    echo '<img src="'.\URL.'upload/'.$value.'" style="margin:20px 0;max-width:400px;max-height:400px" />';
                }
                break;
            case 'TELEFONE':
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" class="form-control telefone" value="'.$value.'" /> ';
                break;
            case 'CELULAR':
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" class="form-control celular" value="'.$value.'" /> ';
                break;
            case 'CPF':
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" class="form-control cpf" value="'.$value.'" /> ';
                break;
            case 'CNPJ':
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" class="form-control cnpj" value="'.$value.'" /> ';
                break;
            case 'CEP':
                if ($onchange == '') {
                    $onchange = ' onchange="onChangeCep(this.value)"';
                }
                echo '<input '.$onchange.' type="text" id="f_'.$name.'" name="'.$name.'" class="form-control cep" value="'.$value.'" /> ';
                break;
            case 'READONLY':
                echo '<input '.$onchange.' type="text" readonly id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
            default:
                $maxLength = $len>0? ' maxlength="'.$len.'"' : '';
                echo '<input '.$onchange.' type="text"'.$maxLength.' id="f_'.$name.'" name="'.$name.'" value="'.$value.'" class="form-control" /> ';
                break;
        }
        echo '</div>';
    }

}
?>
                        </form>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <h2>Fotos</h2>
                        <div id="leilao_fotos"></div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div style="display:flex;flex-wrap:wrap;">
                            <div>
                                <button id="btn_salvar" class="btn btn-primary">Salvar</button>
                                <a href="<?=$baseURL?>" class="btn btn-default">Cancelar</a>
                            </div>
                            <div id="msg_salvar" style="flex:1;line-height:34px;padding: 0 10px;"></div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <a href="<?=\URL?>leilao-lances/<?=$id?>/leilao.csv" target="_blank" class="btn btn-primary">Baixar em CSV</a>
                       <div id="dv_lances"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$appendHTML??'';?>

<script src="<?=\URL?>src/js/leilaofoto.js?v=<?=time()?>"></script>
<script src="<?=\URL?>theme/js/plugins/textarea_autosize/jquery.textarea_autosize.js"></script>
<script src="<?=\URL?>theme/js/plugins/select2/select2.full.min.js"></script>
<script src="<?=\URL?>src/js/lances.js?v=<?=time()?>"></script>
<script>
<?=$js??'';?>
$('#btn_salvar').click(function (e) {
    $('#frm_edit').submit();
});
$('#frm_edit').submit(function (e) {
    e.preventDefault();

    $('#btn_salvar').addClass('disabled');
    $('#btn_salvar').prop('disabled', true);
    $('#btn_salvar').html(`<i class="fa fa-refresh fa-spin"></i> Salvar`);
    $('#msg_salvar').html(`<span class="text-info">Aguarde.</span>`);

    let formData = new FormData(this);
    
    formData.append('fotos', leilaofoto.toJson());

    httpJSON('<?=$urlSave;?>', 'POST', formData)
        .then(res => {
            $('#btn_salvar').removeClass('disabled');
            $('#btn_salvar').prop('disabled', false);
            $('#btn_salvar').html(`Salvar`);
            if (res.status) {
                $('#msg_salvar').html(`<span class="text-success">Salvo com sucesso</span>`);
                window.location = '<?=$baseURL?>';
            } else {
                $('#msg_salvar').html(`<span class="text-danger">${res.message}</span>`);
                setTimeout(() => {
                    $('#msg_salvar').html(``);
                }, 2000);
            }
        })
        .catch(reason => {
            $('#btn_salvar').removeClass('disabled');
            $('#btn_salvar').prop('disabled', false);
            $('#btn_salvar').html(`Salvar`);

            showMsg('#msg_salvar', 'Erro ao salvar', 'fa fa-times', 'danger');
            $('#msg_salvar').html(`<span class="text-danger">Erro ao salvar</span>`);
            setTimeout(() => {
                $('#msg_salvar').html(``);
            }, 2000);
        });

});
$(document).ready(function () {
    leilaofoto.init('#leilao_fotos', <?=$id?>);
<?=$jsOnload ??'';?>
$('textarea.js-auto-size').textareaAutoSize();
lances.init('#dv_lances', <?=$id?>);
});
</script>

