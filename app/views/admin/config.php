<?php

use Core\View;

$baseURL = \URL.'configuracao';

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb('Configurações', $baseURL);

View::setTitle('Configurações');

// View::useComponent('modals');
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
        <h1>Envio de email</h1> <br>

        <div class="row">
            <div class="col-md-6">
                <label for="email_host">Servidor SMTP</label>
                <input type="text" id="email_host" name="email_host" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="email_smtp">Porta SMTP</label>
                <input type="text" id="email_smtp" name="email_smtp" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="email_usuario">Usuário</label>
                <input type="text" id="email_usuario" name="email_usuario" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="email_pass">Senha</label>
                <input type="text" id="email_pass" name="email_pass" class="form-control">
            </div>
        </div>
    </div>
</div>