<?php

use Core\View;
use Core\StringCase;

$baseURL = \URL.'admin/leilao';

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb('Leilão', $baseURL);

View::setTitle('Ver | Leilão');
View::addBreadcrumb('Ver', $baseURL);

// View::useComponent('modals');
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
        <h1><?=$data->nome;?></h1>
        <div class="row">
            <div class="col-md-8 col-xs-12" style="text-align: center;">
                <div class="leilao-img-container">
                <?php if ($fotos->rowCount() == 0) { ?>
                    <img src="<?=\URL?>theme/img/sem-foto.jpg" alt="Sem Foto">
                <?php } else { // ElseIF countFotos ?>
                    <?php $foto = $fotos->fetch(); ?>
                    <div class="leilao-img principal">
                        <img src="<?=\URL?>upload/<?=$foto->imagem_original;?>" alt="<?=$data->nome;?>">
                    </div>
                    <div class="leilao-imgs">
                        <div
                            data-src="<?=\URL.'upload/'.$foto->imagem_original;?>"
                            class="leilao-img thumb">
                            <img src="<?=\URL?>upload/<?=$foto->imagem_thumb;?>" alt="<?=$data->nome;?>">
                        </div>
                        <?php while ($foto = $fotos->fetch()) { ?>
                        <div
                            data-src="<?=\URL.'upload/'.$foto->imagem_original;?>"
                            class="leilao-img thumb">
                            <img
                                src="<?=\URL?>upload/<?=$foto->imagem_thumb;?>"
                                alt="<?=$data->nome;?>">
                        </div>
                        <?php } ?>
                    </div>

                <?php } // Fim IF countFotos ?>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div style="margin: 0 0 20px 0;">
                    <?php 
                    $listaStatus = [
                            ['label' => 'Não publicado', 'style' => 'default'],
                            ['label' => 'Publicado (Aberto)', 'style' => 'warning'],
                            ['label' => 'Em andamento', 'style' => 'success'],
                            ['label' => 'Encerrado', 'style' => 'danger'],
                            ['label' => 'Cancelado', 'style' => 'default'],
                            ['label' => 'Apregoando', 'style' => 'warning']
                        ];
                        $status = isset($listaStatus[$data->status])? $listaStatus[$data->status] : $listaStatus[0];
                        ?>
                    <div class="label label-<?=$status['style'];?>" style="font-size: 2em;">Lote <?=$data->lote;?></div>
                    <span class="text-<?=$status['style'];?>" style="font-size: 2em;margin-left:10px;"><?=$status['label'];?></span>
                </div>
                <div class="clearfix"></div>
                <div style="display:grid;grid-template-columns: auto 1fr;column-gap: 10px;row-gap: 5px;">

                    <div><strong>Início do leilão:</strong></div>
                    <div><?=$data->dateTimeFormat('data_inicio');?></div>
                    
                    <div><strong>Encerramento do leilão:</strong></div>
                    <div><?=$data->dateTimeFormat('data_fim');?></div>
                    
                    <div><strong>Valor inicial:</strong></div>
                    <div>R$ <?=number_format($data->valor_inicial, 2, ',','.');?></div>
                    
                    <div><strong>Incremento:</strong></div>
                    <div>R$ <?=number_format($data->salto_lance, 2, ',','.');?></div>
                    
                    <?php if ($usuarioInicio != false) {?>
                    <div><strong>Iniciado por:</strong></div>
                    <div><a href="<?=\URL.'admin/usuario/'.$usuarioInicio->id;?>"><?=$usuarioInicio->nome;?></a></div>
                    <?php } ?>
                    
                    <?php if ($usuarioFim != false) {?>
                    <div><strong>Encerramento por:</strong></div>
                    <div><a href="<?=\URL.'admin/usuario/'.$usuarioFim->id;?>"><?=$usuarioFim->nome;?></a></div>
                    <?php } ?>
                    
                    <?php if ($arrematadoPor != false) {?>
                    <div><strong>Arremadado por:</strong></div>
                    <div><a href="<?=\URL.'admin/cliente/'.$arrematadoPor->id;?>">
                        <?php
                        if ($arrematadoPor->cnpj != '') {
                            echo $arrematadoPor->id .'# ' . $arrematadoPor->razao_social;
                        } else {
                            echo $arrematadoPor->id .'# ' . $arrematadoPor->nome . ' ' .$arrematadoPor->sobrenome;
                        }
                        ?>
                    </a></div>
                    <div><strong>Valor arrematado:</strong></div>
                    <div>R$ <?=number_format($data->valor_arrematado, 2, ',','.');?></div>
                    <?php } ?>
                    
                    <div>&nbsp;</div>
                    <div>
                        <?php
                        /*
                        $listaStatus = [
                            ['label' => 'Não publicado', 'style' => 'label-default'],
                            ['label' => 'Publicado (Aberto)', 'style' => 'label-warning'],
                            ['label' => 'Em andamento', 'style' => 'label-success'],
                            ['label' => 'Encerrado', 'style' => 'label-danger'],
                            ['label' => 'Cancelado', 'style' => 'label-default']
                        ];
                        $status = isset($listaStatus[$data->status])? $listaStatus[$data->status] : $listaStatus[0];
                        ?>
                        <p><span class="label <?=$status['style'];?>"><?=$status['label'];?></span></p>
                        <?php*/
                        
                        if ($data->status == 0) {
                            echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 1);" class="btn btn-block btn-success"><i class="fa fa-bullhorn"></i> Publicar leilão</button></div>';
                        } elseif ($data->status == 1) {
                            echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 2);" class="btn btn-block btn-default"><i class="fa fa-play"></i> Iniciar leilão</button> ';
                            echo '<button type="button" onclick="setStatusLeilao('.$id.', 4);" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> Cancelar leilão</button></div>';
                        } elseif ($data->status == 2) {
                            echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 5);" class="btn btn-block btn-warning"><i class="fa fa-gavel"></i> Apregoar</button></div>';
                           // echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 3);" class="btn btn-block btn-default"><i class="fa fa-stop"></i> Encerrar leilão</button></div>';
                        } elseif ($data->status == 4) {
                            echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 0);" class="btn btn-block btn-default"><i class="fa fa-undo"></i> Marcar como não publicado</button></div>';
                        } elseif ($data->status == 5) {
                            echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 2);" class="btn btn-block btn-default"><i class="fa fa-gavel"></i> Encerrar pregão</button></div>';
                            // echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 3);" class="btn btn-block btn-default"><i class="fa fa-stop"></i> Encerrar leilão</button></div>';
                        } //elseif ($data->status == 1) {
                        //     echo '<div><button type="button" onclick="setStatusLeilao('.$id.', 5);" class="btn btn-block btn-default"><i class="fa fa-stop"></i> Apregoar</button></div>'; }
                        ?>
                    </div>


                    <div><strong>Parcelas:</strong></div>
                    <div>  <?php if ($data->parcelado == '0') {
                        echo "À vista";
                    } else {
                        echo "Parcelado";
                    } ?>
                   </div> 

                    <div><strong>Processo:</strong></div>
                    <div><p><?=str_replace(["\r\n","\n"],'</p> <p>',$data->processo);?></p></div>

                    <div><strong>Descrição:</strong></div>
                    <div><p><?=str_replace(["\r\n","\n"],'</p> </p>',$data->descricao);?></p></div>
                </div>

                <!-- Lances -->
                <div>
                    <h2>Lances e Parcelas</h2>
                </div>
                
                <?php if ($data->status == 2 || $data->status == 5) { ?>
                <div id="dv_btn_iniciar">
                <button
                    type="button" 
                    onclick="leilaoTimer.iniciarContador(<?=$data->id?>);" 
                    class="btn btn-block btn-default"> Iniciar contador
                </button>
                </div>
                <div id="dv_btn_parar" style="display: none;">
                    <p id="countdown" class="timer"> </p>

                    <button type="button" 
                        onclick="leilaoTimer.pararContador(<?=$data->id?>);" 
                        class="btn btn-block btn-default"> Parar contador
                    </button>
                </div>
                <div id="dv_btn_encerrar">
                    <button
                        type="button"
                        onclick="setStatusLeilao(<?=$id?>, 3);"
                        class="btn btn-block btn-default">
                            <i class="fa fa-stop"></i> Encerrar leilão
                    </button>
                </div>

                <div>

                <div style="display:flex; flex-wrap:wrap;">
                    <div style="flex: 1;padding-right:5px;">
                        <div style="margin-bottom: 5px;"> 
                        <input type="text"
                            class="form-control money" 
                            id="valor_do_lance"  
                            dir='rtl'
                            placeholder ="0,00">
                        </div>

                <div style="flex: 2;">
                    <div>
                    <button data-parcelado="0" class="btn btn-success btn-block btn-fazer-lance">Efetuar lance à vista</button>
                </div>
                <?php } ?>

                <?php if ($data->parcelado == 1) { ?>
                        <button
                            id="btn_lancar_parcela"
                            data-parcelado="1"
                            class="btn btn-success btn-block btn-fazer-lance">Efetuar lance parcelado</button>

                    <?php } else { ?>
                        <button
                            id="btn_lancar_parcela"
                            data-parcelado="1"
                            disabled
                            class="btn btn-default disabled btn-block btn-fazer-lance">Parcela</button>
                    <?php } ?>

                </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="dv_lances" style="overflow: auto;max-height: 400px;"></div>
            </div>
        </div>


    <div class="clearfix"></div>
    <!-- <div class="hr-line-dashed"></div>
    <div style="display:flex;flex-wrap:wrap;">
        <div>
            <a href="<?=$baseURL.'/'.$id."/edit";?>" id="btn_editar" class="btn btn-default"><i class="fa fa-pencil"></i> Editar</a>
            <a href="<?=$baseURL;?>" id="btn_voltar" class="btn btn-default">Voltar</a>
        </div>
    </div> -->
    </div>
</div>
<?=$appendHTML??'';?>
<script src="<?=URL;?>src/js/leilao.js?v=<?=time()?>"></script>
<script src="<?=\URL?>src/js/lances.js?v=<?=time()?>"></script>
<script src="<?=\URL?>src/js/timer.js?v=<?=time()?>"></script>

<script>

function setStatusLeilao (id, statusId) {
    httpJSON(`<?=\URL?>admin/leilao-setstatus/${id}/${statusId}`)
        .then(res => {
            if (res.status) {
                window.location.reload();
            } else {
                alert(res.message);
            }
        })
        .catch(reason => {
            alert('Ocorreu um erro ao mudar o status');
        }); 
}
    $(document).ready(function () {
        lances.init('#dv_lances', <?=$id?>); 

        $('.btn-fazer-lance').click(function () {
            let valor = parseFloat(
            document.querySelector('#valor_do_lance')
            .value
            .replace('.','')
            .replace(',', '.')
            );

            let parcelado = $(this).attr('data-parcelado');
            
            lances.fazerLance(parcelado, valor);
        });
        
        leilaoTimer.aoIniciar = function () {
            $('#dv_btn_iniciar').hide();
            $('#dv_btn_parar').show();
            // $('#dv_btn_encerrar').hide();
        };

        leilaoTimer.aoParar = function () {
            $('#dv_btn_iniciar').show();
            $('#dv_btn_parar').hide();
            // $('#dv_btn_encerrar').hide();
        };

        leilaoTimer.aoEncerrar = function () {
            $('#dv_btn_iniciar').show();
            $('#dv_btn_parar').hide();
            // $('#dv_btn_encerrar').show();
        };
    });
</script>

