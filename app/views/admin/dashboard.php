<?php

use Core\View;

View::setTitle('Dashboard');

// View::addBreadcrumb('Minha Área', '/minha-area');
?>
<div class="row">
    <div class="col-lg-3 col-xs-12 col-md-6">
        <a href="<?=URL?>admin/leilao" style="color: #FFFFFF;">
        <div class="widget style1 blue-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-gavel fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Leilões abertos </span>
                    <h2 class="font-bold"><?=$countAbertos;?></h2>
                </div>
            </div>
        </div></a>
    </div>
    <div class="col-lg-3 col-xs-12 col-md-6">
        <a href="<?=URL?>admin/leilao" style="color: #FFFFFF;">
        <div class="widget style1 blue-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-gavel fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Leilões em andamento </span>
                    <h2 class="font-bold"><?=$countAndamento;?></h2>
                </div>
            </div>
        </div></a>
    </div>
    <div class="col-lg-3 col-xs-12 col-md-6">
        <a href="<?=URL?>leilao-arrematados" style="color: #FFFFFF;">
        <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-handshake-o fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Arremates </span>
                    <h2 class="font-bold"><?=$countArremates;?></h2>
                </div>
            </div>
        </div></a>
    </div>
    <div class="col-lg-3 col-xs-12 col-md-6">
        <a href="<?=URL?>admin/cliente" style="color: #FFFFFF;">
        <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-user-o fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Clientes Ativos</span>
                    <h2 class="font-bold"><?=$countClientes?></h2>
                </div>
            </div>
        </div></a>
    </div>
</div>

<?php function feedActivityList ($record) {
    echo '<div class="feed-activity-list">';
    if ($record->rowCount() == 0) {
        echo '<h2 class="text-center text-muted">Nenhum leilão</h2>';
    } else {
        while($leilao = $record->fetch()) {
            echo '<div class="text-primary">';
            echo '<div class="feed-element">';
            echo '<div class="pull-left">';
            if (is_null($leilao->imagem)) {
                echo '<img src="'.URL.'theme/img/sem-foto.jpg" alt="" class="img-square big" style="opacity:0.3;">';
            } else {
                echo '<img src="'.URL.$leilao->imagem.'" alt="" class="img-square big">';
            }
            echo '</div>';
            echo '<div>';
            echo ' <a href="'.URL.'admin/leilao/'.$leilao->id.'"';
            echo ' style="margin: 0 2px;"';
            echo ' class="btn btn-default pull-right">';
            echo 'Ver leilão';
            echo '</a> ';
            echo ' <a href="'.URL.'admin/leilao/'.$leilao->id.'"';
            echo ' style="margin: 0 2px;"';
            echo ' class="btn btn-primary pull-right" title="Editar leilão">';
            echo '<i class="fa fa-pencil"></i>';
            echo '</a> ';
            echo '<strong>'.$leilao->nome.'</strong>';
            echo '<div style="height:40px;overflow: hidden;">'.$leilao->descricao_breve.'</div>';
            echo '<div class="clearfix"></div>';
            echo '<small class="text-muted">';
            echo 'Inicia em <strong>'.date('d/m/Y H:i:s', strtotime($leilao->data_inicio)).'. </strong>';
            echo 'Termina em <strong>'.date('d/m/Y H:i:s', strtotime($leilao->data_fim)).'.</strong>';
            echo '</small>';
            echo '<div class="clearfix"></div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
    }
    echo '</div>';
} ?>

<div class="row">
    <div class="col-lg-4 col-xs-12 col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Leilões abertos</h5>
            </div>
            <div class="ibox-content">
                <?php feedActivityList($leiloesAbertos); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-xs-12 col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Leilões Em Andamento</h5>
            </div>
            <div class="ibox-content">
                <?php feedActivityList($leiloesAndamento); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-xs-12 col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Novos clientes</h5>
            </div>
            <div class="ibox-content">
                <?php 
                    echo '<div class="feed-activity-list">';
                    if ($novosClientes->rowCount() == 0) {
                        echo '<h2 class="text-center text-muted">Nenhum novo cliente</h2>';
                    } else {
                        while($cliente = $novosClientes->fetch()) {
                            echo '<a href="'.URL.'admin/cliente/'.$cliente->id.'" class="text-primary">';
                            echo '<div class="feed-element">';
                            echo '<div class="pull-left">';
                            if (is_null($cliente->imagem)) {
                                echo '<img src="'.URL.'theme/img/sem-foto.jpg" alt="" class="img-circle" style="opacity:0.3;">';
                            } else {
                                echo '<img src="'.URL.$cliente->imagem.'" alt="" class="img-circle">';
                            }
                            echo '</div>';
                            echo '<div>';
                            echo '<strong>'.$cliente->nome.' '.$cliente->sobrenome.'</strong>';
                            echo '<div style="height:40px;overflow: hidden;">'.$cliente->celular.' - '.$cliente->email.'<br />';
                            echo 'Cadastrado em <strong>'.date('d/m/Y H:i:s', strtotime($cliente->data_create)).'</strong>';
                            echo '</div>';
                            echo '</div>';

                            echo '</div>';
                            echo '</a>';
                        }
                    }
                    echo '</div>'; ?>
            </div>
        </div>
    </div>
</div>