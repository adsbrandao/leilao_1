<?php

use Core\View;
use Core\StringCase;
use Core\Auth;

$baseURL = \URL.'leilao';

View::addBreadcrumb('Dashboard', \URL.'');
View::addBreadcrumb('Leilões', $baseURL);

View::setTitle('Ver | Leilão');
View::addBreadcrumb('Ver', $baseURL);

$userData = Auth::get();

$permiteLances = $userData->usuario_status == 1 || $userData->cliente_status == 1;
// $contador = $userData->usuario_status == 1;

// View::useComponent('modals');
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
        <h1><?=$data->nome;?></h1>
        <div class="row">
            <div class="col-md-8 col-xs-12" style="text-align: center;">
                <div class="leilao-img-container">
                <?php if ($fotos->rowCount() == 0) { ?>
                    <img src="<?=\URL?>theme/img/sem-foto.jpg" alt="Sem Foto">
                <?php } else { // ElseIF countFotos ?>
                    <?php $foto = $fotos->fetch(); ?>
                    <div class="leilao-img principal">
                        <img src="<?=\URL?>upload/<?=$foto->imagem_original;?>" alt="<?=$data->nome;?>">
                    </div>
                    <div class="leilao-imgs">
                        <div
                            data-src="<?=\URL.'upload/'.$foto->imagem_original;?>"
                            class="leilao-img thumb">
                            <img src="<?=\URL?>upload/<?=$foto->imagem_thumb;?>" alt="<?=$data->nome;?>">
                        </div>
                        <?php while ($foto = $fotos->fetch()) { ?>
                        <div
                            data-src="<?=\URL.'upload/'.$foto->imagem_original;?>"
                            class="leilao-img thumb">
                            <img
                                src="<?=\URL?>upload/<?=$foto->imagem_thumb;?>"
                                alt="<?=$data->nome;?>">
                        </div>
                        <?php } ?>
                    </div>

                <?php } // Fim IF countFotos ?>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div style="margin: 0 0 20px 0;">
                    <?php 
                    $listaStatus = [
                            0 => ['label' => 'Não publicado', 'style' => 'default'],
                            1 => ['label' => 'Publicado (Aberto)', 'style' => 'warning'],
                            2 => ['label' => 'Em andamento', 'style' => 'success'],
                            3 => ['label' => 'Encerrado', 'style' => 'danger'],
                            4 => ['label' => 'Cancelado', 'style' => 'default'],
                            5 => ['label' => 'Apregoando', 'style' => 'warning']
                        ];
                        $status = isset($listaStatus[$data->status])? $listaStatus[$data->status] : $listaStatus[0];
                        ?>
                    <div class="label label-<?=$status['style'];?>" style="font-size: 2em;">Lote <?=$data->lote;?></div>
                    <span class="text-<?=$status['style'];?>" style="font-size: 2em;margin-left:10px;"><?=$status['label'];?></span>
                </div>
                <div class="clearfix"></div>
                <div style="display:grid;grid-template-columns: auto 1fr;column-gap: 10px;row-gap: 5px;">

                    <div><strong>Início do leilão:</strong></div>
                    <div><?=$data->dateTimeFormat('data_inicio');?></div>
                    
                    <div><strong>Encerramento do leilão:</strong></div>
                    <div><?=$data->dateTimeFormat('data_fim');?></div>
                    
                    <div><strong>Valor inicial:</strong></div>
                    <div>R$ <?=number_format($data->valor_inicial, 2, ',','.');?></div>
                    
                    <div><strong>Incremento:</strong></div>
                    <div>R$ <?=number_format($data->salto_lance, 2, ',','.');?></div>
                    
                    <?php /*if ($usuarioInicio != false) {?>
                    <div><strong>Iniciado por:</strong></div>
                    <div><a href="<?=\URL.'admin/usuario/'.$usuarioInicio->id;?>"><?=$usuarioInicio->nome;?></a></div>
                    <?php }*/ ?>
                    
                    <?php /* if ($usuarioFim != false) { ?>
                    <div><strong>Encerramento por:</strong></div>
                    <div><?=$usuarioFim->nome;?></div>
                    <?php }*/ ?>
                    
                    <?php if ($data->arrematado_por != false) {?>
                    <div><strong>Arremadado por:</strong></div>
                    <div><?php
                        if ($arrematadoPor->cnpj != '') {
                            echo $arrematadoPor->razao_social;
                        } else {
                            echo $arrematadoPor->nome . ' ' .$arrematadoPor->sobrenome;
                        }
                        ?></div>

                    <div><strong>Valor arrematado:</strong></div>
                    <div>R$ <?=number_format($data->valor_arrematado, 2, ',','.');?></div>
                    <?php } ?>

                    <div><strong>Parcelas:</strong></div>
                    <div>  <?php if ($data->parcelado == '0') {
                        echo "À vista";
                    } else {
                        echo "Parcelado";
                    } ?>
                   </div> 
                    

                    
                    <div><strong>Processo:</strong></div>
                    <div><p><?=str_replace(["\r\n","\n"],'</p> <p>',$data->processo);?></p></div>

                    <div><strong>Descrição:</strong></div>
                    <div><p><?=str_replace(["\r\n","\n"],'</p> </p>',$data->descricao);?></p></div>
                </div>

                <!-- Lances -->
                <div>
                    <h2>Lances e Parcelas</h2>
                </div>
                
                <p id="countdown" class="timer"> </p>

                <?php if (($data->status == 2 || $data->status == 5) && $permiteLances) { ?>

                <div> 
                <div style="display:flex; flex-wrap:wrap;">
                <div style="flex: 1;padding-right:5px;">
                <div style="margin-bottom: 5px;"> 
                <input type="text"
                    class="form-control money" 
                    id="valor_do_lance"  
                    placeholder ="0,00">
                </div> </div> 

                <div style="flex: 2;">
                <div>
                    <button data-parcelado="0" class="btn btn-success btn-block btn-fazer-lance">Efetuar lance à vista</button>
                </div>
                <?php } ?>
                <div>
                    <?php if ($data->parcelado == 1) { ?>
                        <button
                            id="btn_lancar_parcela"
                            data-parcelado="1"
                            class="btn btn-success btn-block btn-fazer-lance">Efetuar lance parcelado</button>

                    <?php } else { ?>
                        <button
                            id="btn_lancar_parcela"
                            data-parcelado="1"
                            disabled
                            class="btn btn-default disabled btn-block btn-fazer-lance">Leilão à vista</button>
                    <?php } ?>
                </div>
                </div>
                </div>
                </div>
                <div class="clearfix"></div>
                <div id="dv_lances" style="overflow: auto;max-height: 400px;"></div>

            </div>
        </div>
    <div class="clearfix"></div>
    </div>
</div>

<?=$appendHTML??'';?>
<script src="<?=URL;?>src/js/leilao.js?v=<?=time()?>"></script>
<script src="<?=\URL?>src/js/lances.js?v=<?=time()?>"></script>
<script src="<?=\URL?>src/js/timer.js?v=<?=time()?>"></script>

<script>
    $(document).ready(function () {
        lances.init('#dv_lances', <?=$id?>);

        $('.btn-fazer-lance').click(function () {
            let valor = parseFloat(
             document.querySelector('#valor_do_lance')
             .value
            .replace('.','')
            .replace(',', '.')
            );

            let parcelado = $(this).attr('data-parcelado');

            lances.fazerLance(parcelado, valor);
            });

            leilaoTimer.aoIniciar = function () {
            console.log('Mostrar inicio');
            };

            leilaoTimer.aoParar = function () {
            console.log('Mostrar finalização');
            };
        });
</script>