<?php session_start();
ob_start();

use Core\Query;
use Core\View;

include 'vendor/autoload.php';
include 'config.php';

Query::setConnection(
    DB_NAME,
    null,
    DB_HOST,
    DB_PORT,
    DB_USER,
    DB_PASS,
    'mysql');
