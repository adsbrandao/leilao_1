<?php

use Core\Router;

Router::get('index','/admin', 'Admin/Dashboard@index');

Router::get('clientes_ativos', '/admin/cliente/ativos', 'Admin/Cliente@getClientesAtivos');
Router::resource('/admin/usuario', 'Admin/Usuario');
Router::resource('/admin/cliente', 'Admin/Cliente');
Router::resource('/admin/categoria', 'Admin/Categoria');

// Leilao
Router::resource('/admin/leilao', 'Admin/Leilao');
Router::get('foto_tmp', '/admin/leilao-foto/tmp/:key/:tipo', 'Admin/LeilaoFoto@readTmp');
Router::get('leilao_limpar', '/admin/leilao-foto/limpar', 'Admin/LeilaoFoto@limpar');
Router::get('leilao_foto', '/admin/leilao-foto/:id', 'Admin/LeilaoFoto@fotosLeilao');
Router::post('leilao_foto_upload', '/admin/leilao-foto/upload/:id', 'Admin/LeilaoFoto@upload');
Router::get('leilao_setstatus', '/admin/leilao-setstatus/:id/:status', 'Leilao@setStatus');

Router::get('leilao_iniciarcontador', '/admin/leilao-iniciarcontador/:id', 'Leilao@iniciarContador');
Router::get('leilao_pararcontador', '/admin/leilao-pararcontador/:id', 'Leilao@pararContador');

Router::get('leilao_lances','/leilao-lances/:id/leilao.csv', 'Leilao@lancesCSV');

// Configuração Geral da plataforma
Router::get('configuracao', '/configuracao', 'Admin/Configuracao@index');
Router::post('configuracao_update', '/configuracao', 'Admin/Configuracao@update');

// Pagamento
Router::get('cfg_pagamento', '/pagamento-config', 'Admin/Configuracao@pagamento');
Router::post('cfg_pagamento_update', '/pagamento-config', 'Admin/Configuracao@pagamentoUpdate');