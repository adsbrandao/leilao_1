<?php

use Core\Router;

Router::get('index','/', 'Conta@login');
// Leiloes
// Lista de leiloes ativos
Router::get('leiloes','/leilao', 'Leilao@index');
Router::get('leiloes_arrematados','/leilao-arrematados', 'Leilao@arrematados');
Router::get('leiloes_cancelados','/leilao-cancelados', 'Leilao@cancelados');

// Detalhe do leilao ou lote
Router::get('leilao','/leilao/:id', 'Leilao@show');

// Pegar lista de [ultimos] lances
Router::get('leilao_lances','/leilao-lances/:id', 'Leilao@lances');

// LOGIN ---------------------------------

Router::get('cadastro','/cadastro', 'Conta@cadastro');
Router::post('cadastro','/cadastro', 'Conta@cadastroAction');
Router::get('valida_cadastro','/valida-cadastro/:code?', 'Conta@validar');

Router::get('login','/login', 'Conta@login');
Router::post('login','/login', 'Conta@loginAction');

Router::get('esqueci_senha','/esqueci-senha', 'Conta@esqueciSenha');
Router::post('esqueci_senha','/esqueci-senha', 'Conta@esqueciSenhaAction');

Router::post('teste_cadastro','/teste-cadastro', 'Conta@testeCadastro');

Router::get('logout','/logout', function () {
    \Core\Auth::logout();
    \Core\ResponseSimple::redirect(\URL);
});
// marco@polo.inc

