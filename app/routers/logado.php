<?php

use Core\Router;
use Core\Auth;

if (Auth::isCliente()) {
   include(PATH . 'app/routers/clientes.php');
}

if (Auth::isUsuario()) {
   include(PATH . 'app/routers/admin.php');
}

// Dar lance
Router::post('leilao_lance','/leilao-lances/:id', 'Leilao@darLance');
