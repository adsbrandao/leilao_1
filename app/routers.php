<?php

use Core\Router;
use Core\Auth;
use Core\ResponseSimple;

include(\PATH . 'app/routers/geral.php');

if (Auth::isAuth()) {
    include(\PATH . 'app/routers/logado.php');
} else {
    // ResponseSimple::redirect(\URL);
}