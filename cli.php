#!/usr/bin/php
<?php
define('MODO', 'CLI');
include('app/bootstrap.php');

$funcoes = [
    'help',
    'generate',
    'resource',
    'controller'
];

$fnc = 'help';
$params = [];

if ($argv) {
    foreach ($argv as $k=>$v)
    {
        if ($k==0) continue;
        if ($k==1){
            $fnc = $v;
            continue;
        }
        $it = explode("=",$argv[$k]);
        if (isset($it[1])) {
            $params[$it[0]] = $it[1];
        } else {
            $params[] = $it[0];
        }
    }
}

if (!in_array($fnc, $funcoes)) {
    $fnc = 'help';
}

include('core/command/'.$fnc.".php");
