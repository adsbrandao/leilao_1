-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: leilao_novo
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblcliente`
--

DROP TABLE IF EXISTS `tblcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblcliente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` int NOT NULL DEFAULT '2' COMMENT '0 - INATIVO / 1 - ATIVO / 2 - NOVO',
  `imagem` varchar(255) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `sobrenome` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `nascimento` date NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `razao_social` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `cep` varchar(10) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` varchar(50) NOT NULL,
  `complemento` varchar(100) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `data_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_update` datetime NOT NULL,
  `data_acesso` datetime NOT NULL,
  `documento_frete` varchar(255) DEFAULT NULL,
  `documento_verso` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcliente`
--

LOCK TABLES `tblcliente` WRITE;
/*!40000 ALTER TABLE `tblcliente` DISABLE KEYS */;
INSERT INTO `tblcliente` VALUES (1,2,NULL,'Jonas','Ribeiro','(31)9996-6655','(31)99999-9999','cliente@leilao.com','e10adc3949ba59abbe56e057f20f883e','1990-03-04','093.627.416-60','','','30140-070','Rua Aimorés','100','CS','Funcionarios','MG','Belo Horizonte','2021-03-05 14:01:15','2021-03-09 15:32:10','2021-03-05 11:01:15',NULL,NULL),(4,1,NULL,'Marco','Polo','(31)9988-7766','(31)98765-3214','marco@polo.inc','e10adc3949ba59abbe56e057f20f883e','1990-01-01','093.627.416-60','','','30140-070','Rua Aimorés','1234','CS','Funcionários','MG','Belo Horizonte','2021-03-09 18:28:47','2021-03-09 15:29:04','2021-03-09 15:28:47',NULL,NULL),(5,2,NULL,'Cliente 2','Teste','(31)9999-6666','(31)33332-2222','cliente2@leilao.com','e10adc3949ba59abbe56e057f20f883e','1990-03-04','974.251.170-53','','','30140-070','Rua Aimorés','123','CS','Funcionários','MG','Belo Horizonte','2021-03-11 12:33:54','2021-03-11 09:33:54','2021-03-11 09:33:54',NULL,NULL),(6,2,NULL,'Gonsales','Lopes','(31)9999-9999','(31)99999-9999','cliente3@leilao.com','e10adc3949ba59abbe56e057f20f883e','2000-02-01','','01.615.814/0068-00','Unilever','06210-970','Rua Oswaldo Collino, 267 ','1','Galpão','Presidente Altino ','SP','Osasco','2021-03-11 13:04:32','2021-03-11 10:04:32','2021-03-11 10:04:32',NULL,NULL);
/*!40000 ALTER TABLE `tblcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblgrupo`
--

DROP TABLE IF EXISTS `tblgrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblgrupo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblgrupo`
--

LOCK TABLES `tblgrupo` WRITE;
/*!40000 ALTER TABLE `tblgrupo` DISABLE KEYS */;
INSERT INTO `tblgrupo` VALUES (1,'Administrador',1),(2,'Leiloeiro',1),(3,'Usuario',1);
/*!40000 ALTER TABLE `tblgrupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbllance`
--

DROP TABLE IF EXISTS `tbllance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbllance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int DEFAULT NULL,
  `cliente_id` int DEFAULT NULL,
  `leilao_id` int DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbllance_1_FK` (`usuario_id`),
  KEY `tbllance_2_FK` (`cliente_id`),
  KEY `tbllance_FK` (`leilao_id`),
  CONSTRAINT `tbllance_1_FK` FOREIGN KEY (`usuario_id`) REFERENCES `tblusuario` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tbllance_2_FK` FOREIGN KEY (`cliente_id`) REFERENCES `tblcliente` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tbllance_FK` FOREIGN KEY (`leilao_id`) REFERENCES `tblleilao` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbllance`
--

LOCK TABLES `tbllance` WRITE;
/*!40000 ALTER TABLE `tbllance` DISABLE KEYS */;
INSERT INTO `tbllance` VALUES (2,'2021-03-11 19:18:00',NULL,1,2,500.99),(3,'2021-03-11 19:38:49',1,NULL,2,600.00),(6,'2021-03-11 20:06:13',2,NULL,8,1100.00),(7,'2021-03-11 20:06:15',2,NULL,8,1600.00),(8,'2021-03-11 20:08:58',2,NULL,8,2100.00),(9,'2021-03-11 20:08:59',2,NULL,8,2600.00),(10,'2021-03-11 20:09:26',2,NULL,8,3100.00),(11,'2021-03-11 20:09:27',2,NULL,8,3600.00),(12,'2021-03-11 20:09:27',2,NULL,8,4100.00),(13,'2021-03-11 20:09:28',2,NULL,8,4600.00),(14,'2021-03-11 20:09:28',2,NULL,8,5100.00),(15,'2021-03-11 20:30:36',2,NULL,8,5600.00),(16,'2021-03-11 20:30:37',2,NULL,8,6100.00),(17,'2021-03-11 20:30:37',2,NULL,8,6600.00),(18,'2021-03-11 20:30:38',2,NULL,8,7100.00),(19,'2021-03-11 20:31:14',2,NULL,8,7600.00),(20,'2021-03-11 20:33:18',2,NULL,4,500.00),(21,'2021-03-11 20:33:19',2,NULL,4,1000.00),(22,'2021-03-11 20:33:19',2,NULL,4,1500.00),(23,'2021-03-11 20:33:20',2,NULL,4,2000.00),(24,'2021-03-11 20:33:20',2,NULL,4,2500.00),(25,'2021-03-11 20:33:21',2,NULL,4,3000.00),(26,'2021-03-11 20:33:23',2,NULL,4,3500.00),(27,'2021-03-11 20:35:49',2,NULL,4,4000.00),(28,'2021-03-11 20:35:52',2,NULL,4,4500.00),(29,'2021-03-11 20:36:17',2,NULL,4,5000.00),(30,'2021-03-11 20:36:19',2,NULL,4,5500.00),(31,'2021-03-11 20:36:22',2,NULL,4,6000.00),(32,'2021-03-11 20:36:55',2,NULL,4,6500.00),(33,'2021-03-11 20:37:15',2,NULL,4,NULL),(34,'2021-03-11 20:37:22',2,NULL,4,15000.00),(35,'2021-03-11 20:37:23',2,NULL,4,15500.00),(36,'2021-03-11 20:37:24',2,NULL,4,16000.00),(37,'2021-03-11 20:37:24',2,NULL,4,16500.00),(38,'2021-03-11 20:37:25',2,1,4,17000.00);
/*!40000 ALTER TABLE `tbllance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblleilao`
--

DROP TABLE IF EXISTS `tblleilao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblleilao` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` int NOT NULL DEFAULT '0' COMMENT '0 - Não publicado / 1 - Publicado (Aberto) / 2 - Em andamento  / 3 - Encerrado / 4 - Cancelado',
  `data_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_update` datetime NOT NULL,
  `data_inicio` datetime NOT NULL,
  `data_fim` datetime NOT NULL,
  `lote` varchar(100) DEFAULT '1',
  `usuario_inicio` int DEFAULT NULL,
  `usuario_fim` int DEFAULT NULL,
  `arrematado_por` int DEFAULT NULL,
  `categoria_id` int NOT NULL,
  `nome` varchar(255) NOT NULL,
  `processo` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `descricao` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `salto_lance` decimal(10,2) DEFAULT '50.00' COMMENT 'Salto por lance',
  `valor_inicial` decimal(20,2) DEFAULT NULL,
  `valor_arrematado` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tblleilao_1_FK` (`usuario_inicio`),
  KEY `tblleilao_2_FK` (`usuario_fim`),
  KEY `tblleilao_3_FK` (`categoria_id`),
  KEY `tblleilao_4_FK` (`arrematado_por`),
  CONSTRAINT `tblleilao_1_FK` FOREIGN KEY (`usuario_inicio`) REFERENCES `tblusuario` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tblleilao_2_FK` FOREIGN KEY (`usuario_fim`) REFERENCES `tblusuario` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tblleilao_3_FK` FOREIGN KEY (`categoria_id`) REFERENCES `tblleilao_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblleilao_4_FK` FOREIGN KEY (`arrematado_por`) REFERENCES `tblcliente` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblleilao`
--

LOCK TABLES `tblleilao` WRITE;
/*!40000 ALTER TABLE `tblleilao` DISABLE KEYS */;
INSERT INTO `tblleilao` VALUES (1,1,'2021-03-05 19:02:19','2021-03-11 14:28:47','2021-03-10 00:00:00','2021-03-11 00:00:00','99',2,NULL,NULL,1,'Teste','teste','teste',50.00,1000.00,NULL),(2,3,'2021-03-09 19:15:08','2021-03-11 14:30:29','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,NULL,NULL,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,NULL),(3,2,'2021-03-05 19:02:19','2021-03-11 14:28:47','2021-03-10 00:00:00','2021-03-11 00:00:00','99',2,NULL,NULL,1,'Teste','teste','teste',50.00,1000.00,NULL),(4,3,'2021-03-09 19:15:08','2021-03-11 14:30:29','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,2,1,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,17000.00),(5,0,'2021-03-09 19:15:08','2021-03-11 14:30:29','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,NULL,NULL,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,NULL),(6,4,'2021-03-09 19:15:08','2021-03-11 14:30:29','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,NULL,NULL,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,NULL),(7,3,'2021-03-09 19:15:08','2021-03-11 14:30:29','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,NULL,NULL,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,NULL),(8,3,'2021-03-09 19:15:08','2021-03-11 17:42:37','2021-04-01 16:14:00','2021-03-31 16:14:00','1',2,2,NULL,1,'Fox 2010 32bits','Carro super novo equipado com windows XP','Window XP e Office',500.00,15000.00,7600.00);
/*!40000 ALTER TABLE `tblleilao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblleilao_categoria`
--

DROP TABLE IF EXISTS `tblleilao_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblleilao_categoria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pai` int DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tblleilao_categoria_1_FK` (`pai`),
  CONSTRAINT `tblleilao_categoria_1_FK` FOREIGN KEY (`pai`) REFERENCES `tblleilao_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblleilao_categoria`
--

LOCK TABLES `tblleilao_categoria` WRITE;
/*!40000 ALTER TABLE `tblleilao_categoria` DISABLE KEYS */;
INSERT INTO `tblleilao_categoria` VALUES (1,NULL,'Sem categoria',NULL),(2,NULL,'Veículos',NULL),(3,2,'Carros',NULL),(4,2,'Motos',NULL),(5,NULL,'Imóveis',NULL),(6,5,'Rurais',NULL),(7,5,'Apartamentos',NULL),(8,5,'Casas','cat-81615319750.png'),(9,NULL,'Máquinas','cat--1615319867.png');
/*!40000 ALTER TABLE `tblleilao_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblleilao_foto`
--

DROP TABLE IF EXISTS `tblleilao_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblleilao_foto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `leilao_id` int DEFAULT NULL,
  `ordem` int NOT NULL DEFAULT '0',
  `imagem_original` varchar(255) DEFAULT NULL,
  `imagem_thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tblleilao_foto_FK` (`leilao_id`),
  CONSTRAINT `tblleilao_foto_FK` FOREIGN KEY (`leilao_id`) REFERENCES `tblleilao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblleilao_foto`
--

LOCK TABLES `tblleilao_foto` WRITE;
/*!40000 ALTER TABLE `tblleilao_foto` DISABLE KEYS */;
INSERT INTO `tblleilao_foto` VALUES (7,1,0,'leilao-1-0-1615483727.jpg','leilao-t-1-0-1615483727.jpg'),(9,2,0,'leilao-2-0-1615483829.jpg','leilao-t-2-0-1615483829.jpg'),(10,8,0,'leilao-8-0-1615488406.jpg','leilao-t-8-0-1615488406.jpg'),(11,8,0,'leilao-8-1-1615488438.jpg','leilao-t-8-1-1615488438.jpg');
/*!40000 ALTER TABLE `tblleilao_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmenu_lateral`
--

DROP TABLE IF EXISTS `tblmenu_lateral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblmenu_lateral` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `caminho` varchar(100) DEFAULT NULL,
  `icone` varchar(100) DEFAULT NULL,
  `pai` int DEFAULT NULL,
  `permissao_id` int DEFAULT NULL,
  `ordem` int DEFAULT '0',
  `permite_cliente` int DEFAULT '0' COMMENT '0 - Para usuarios / 1 - Para clientes ativos / 2 - Todos',
  PRIMARY KEY (`id`),
  KEY `tblmenu_lateral_1_FK` (`pai`),
  CONSTRAINT `tblmenu_lateral_1_FK` FOREIGN KEY (`pai`) REFERENCES `tblmenu_lateral` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmenu_lateral`
--

LOCK TABLES `tblmenu_lateral` WRITE;
/*!40000 ALTER TABLE `tblmenu_lateral` DISABLE KEYS */;
INSERT INTO `tblmenu_lateral` VALUES (1,'Dashboard','/admin','fa-th-large',NULL,1,0,0),(2,'Clientes','/admin/cliente','fa-users',NULL,10,4,0),(3,'Leilões','/admin/leilao','fa-gavel',NULL,NULL,2,1),(4,'Categorias','/admin/categoria','fa-th-list',3,18,2,0),(5,'Usuários','/admin/usuario','fa-user-circle-o',NULL,2,5,0),(6,'Configuração','/configuracao','fa-cog',NULL,42,6,0),(7,'Geral','/configuracao','fa-cog',6,26,5,0),(8,'Configuração de pagamento','/pagamento-config','fa-money',6,44,6,0),(9,'Minha área','/minha-area','fa-th-large',NULL,50,0,2),(10,'Minha Conta','/minha-area/conta','fa-user',NULL,50,1,2),(11,'Meus Arremates','/minha-area/arremates',NULL,3,50,5,1),(12,'Leilões Ativos','/leilao',NULL,3,NULL,3,2),(13,'Cadastro dos Leilões','/admin/leilao',NULL,3,26,1,0),(14,'Leilões Arrematados','/leilao-arrematados',NULL,3,NULL,4,2),(15,'Leilões Cancelados','/leilao-cancelados',NULL,3,NULL,5,2);
/*!40000 ALTER TABLE `tblmenu_lateral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpermissao`
--

DROP TABLE IF EXISTS `tblpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblpermissao` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `request` varchar(255) NOT NULL,
  `method` varchar(50) NOT NULL DEFAULT 'any',
  `permite_cliente` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpermissao`
--

LOCK TABLES `tblpermissao` WRITE;
/*!40000 ALTER TABLE `tblpermissao` DISABLE KEYS */;
INSERT INTO `tblpermissao` VALUES (1,'admin-dashboard','/admin','any',0),(2,'usuario','/admin/usuario','GET',0),(3,'usuario-store','/admin/usuario','POST',0),(4,'usuario-create','/admin/usuario/create','GET',0),(5,'usuario-show','/admin/usuario/:id','GET',0),(6,'usuario-update','/admin/usuario/:id','PUT',0),(7,'usuario-update','/admin/usuario/:id','POST',0),(8,'usuario-delete','/admin/usuario/:id','DELETE',0),(9,'usuario-edit','/admin/usuario/:id/edit','GET',0),(10,'cliente','/admin/cliente','GET',0),(11,'cliente-store','/admin/cliente','POST',0),(12,'cliente-create','/admin/cliente/create','GET',0),(13,'cliente-show','/admin/cliente/:id','GET',0),(14,'cliente-update','/admin/cliente/:id','PUT',0),(15,'cliente-update','/admin/cliente/:id','POST',0),(16,'cliente-delete','/admin/cliente/:id','DELETE',0),(17,'cliente-edit','/admin/cliente/:id/edit','GET',0),(18,'categoria','/admin/categoria','GET',0),(19,'categoria-store','/admin/categoria','POST',0),(20,'categoria-create','/admin/categoria/create','GET',0),(21,'categoria-show','/admin/categoria/:id','GET',0),(22,'categoria-update','/admin/categoria/:id','PUT',0),(23,'categoria-update','/admin/categoria/:id','POST',0),(24,'categoria-delete','/admin/categoria/:id','DELETE',0),(25,'categoria-edit','/admin/categoria/:id/edit','GET',0),(26,'leilao','/admin/leilao','GET',0),(27,'leilao-store','/admin/leilao','POST',0),(28,'leilao-create','/admin/leilao/create','GET',0),(29,'leilao-show','/admin/leilao/:id','GET',0),(30,'leilao-update','/admin/leilao/:id','PUT',0),(31,'leilao-update','/admin/leilao/:id','POST',0),(32,'leilao-delete','/admin/leilao/:id','DELETE',0),(33,'leilao-edit','/admin/leilao/:id/edit','GET',0),(34,'leilao-lote','/admin/leilao-lote','GET',0),(35,'leilao-lote-store','/admin/leilao-lote','POST',0),(36,'leilao-lote-create','/admin/leilao-lote/create','GET',0),(37,'leilao-lote-show','/admin/leilao-lote/:id','GET',0),(38,'leilao-lote-update','/admin/leilao-lote/:id','PUT',0),(39,'leilao-lote-update','/admin/leilao-lote/:id','POST',0),(40,'leilao-lote-delete','/admin/leilao-lote/:id','DELETE',0),(41,'leilao-lote-edit','/admin/leilao-lote/:id/edit','GET',0),(42,'configuracao','/configuracao','GET',0),(43,'configuracao','/configuracao','POST',0),(44,'cfg_pagamento','/pagamento-config','GET',0),(45,'cfg_pagamento','/pagamento-config','POST',0),(46,'minha-area','/minha-area','GET',2),(47,'minha-conta','/minha-area/conta','GET',2),(48,'minha-conta-update','/minha-area/conta','POST',2),(49,'meus-arremates','/minha-area/arremates','GET',1),(50,'somente-cliente','/','ANY',0);
/*!40000 ALTER TABLE `tblpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpermissao_grupo`
--

DROP TABLE IF EXISTS `tblpermissao_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblpermissao_grupo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `grupo_id` int DEFAULT NULL,
  `permissao_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tblpermissao_grupo_1_FK` (`grupo_id`),
  KEY `tblpermissao_grupo_2_FK` (`permissao_id`),
  CONSTRAINT `tblpermissao_grupo_1_FK` FOREIGN KEY (`grupo_id`) REFERENCES `tblgrupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblpermissao_grupo_2_FK` FOREIGN KEY (`permissao_id`) REFERENCES `tblpermissao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpermissao_grupo`
--

LOCK TABLES `tblpermissao_grupo` WRITE;
/*!40000 ALTER TABLE `tblpermissao_grupo` DISABLE KEYS */;
INSERT INTO `tblpermissao_grupo` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,1,14),(15,1,15),(16,1,16),(17,1,17),(18,1,18),(19,1,19),(20,1,20),(21,1,21),(22,1,22),(23,1,23),(24,1,24),(25,1,25),(26,1,26),(27,1,27),(28,1,28),(29,1,29),(30,1,30),(31,1,31),(32,1,32),(33,1,33),(34,1,34),(35,1,35),(36,1,36),(37,1,37),(38,1,38),(39,1,39),(40,1,40),(41,1,41),(42,1,42),(43,1,43),(44,1,44),(45,1,45),(46,2,1),(47,2,2),(48,2,3),(49,2,4),(50,2,5),(51,2,6),(52,2,7),(53,2,8),(54,2,9),(55,2,10),(56,2,11),(57,2,12),(58,2,13),(59,2,14),(60,2,15),(61,2,16),(62,2,17),(63,2,18),(64,2,19),(65,2,20),(66,2,21),(67,2,22),(68,2,23),(69,2,24),(70,2,25),(71,2,26),(72,2,27),(73,2,28),(74,2,29),(75,2,30),(76,2,31),(77,2,32),(78,2,33),(79,2,34),(80,2,35),(81,2,36),(82,2,37),(83,2,38),(84,2,39),(85,2,40),(86,2,41),(87,3,1),(88,3,26),(89,3,27),(90,3,28),(91,3,29),(92,3,30),(93,3,31),(94,3,32),(95,3,33),(96,3,34),(97,3,35),(98,3,36),(99,3,37),(100,3,38),(101,3,39),(102,3,40),(103,3,41);
/*!40000 ALTER TABLE `tblpermissao_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusuario`
--

DROP TABLE IF EXISTS `tblusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tblusuario` (
  `id` int NOT NULL AUTO_INCREMENT,
  `grupo_id` int DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `imagem` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `celular` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `data_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_update` datetime DEFAULT NULL,
  `data_acesso` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tblusuario_1_FK` (`grupo_id`),
  CONSTRAINT `tblusuario_1_FK` FOREIGN KEY (`grupo_id`) REFERENCES `tblgrupo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuario`
--

LOCK TABLES `tblusuario` WRITE;
/*!40000 ALTER TABLE `tblusuario` DISABLE KEYS */;
INSERT INTO `tblusuario` VALUES (1,1,1,NULL,'Administrador','','','admin@leilao.com','e10adc3949ba59abbe56e057f20f883e','2021-03-08 13:58:00',NULL,NULL),(2,1,1,NULL,'Jonas Silva','','','jonasribeiro19@gmail.com','e10adc3949ba59abbe56e057f20f883e','2021-03-08 16:58:00',NULL,NULL),(3,1,2,NULL,'Teste Leiloeiro','','','leiloeiro@leilao.com','e10adc3949ba59abbe56e057f20f883e','2021-03-08 16:58:00',NULL,NULL),(4,1,2,NULL,'Teste Usuario','','','usuario@leilao.com','e10adc3949ba59abbe56e057f20f883e','2021-03-08 16:58:00',NULL,NULL),(7,1,1,NULL,'Daniel','(31)11111-1111','(31)11111-11','daniel@gmail.com','e10adc3949ba59abbe56e057f20f883e','2021-03-09 20:10:22','2021-03-09 17:10:22','2021-03-09 17:10:22');
/*!40000 ALTER TABLE `tblusuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-12  8:27:22
